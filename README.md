#fortis-web

This is the official Fortis web property created with [Symfony 2](http://symfony.com/doc/2.0/index.html) on September 12, 2015, 4:55 pm.

##Getting Started

This section will guide you through setting up your windows system for remote development of the project.

----------

### Installing The Dependencies

*Note: When given the option, always allow the PATH to be updated*

#### 1. Microsoft Visual Studio c++ build tools 2015

Download and run the *2015 version* from http://landinghub.visualstudio.com/visual-cpp-build-tools

Select at minimum the 8.1 SDK and .NET SDK on install.

#### 2. Node

Download and run the *Latest Stable Release (LTS)* from https://nodejs.org/en/

*Note: You may need to run `npm config set msvs_version 2015 --global` for node to recognize the c++ build tools from the previous step.*

#### 3. Python

Download and run the *Windows x86-64 executable installer* from https://www.python.org/downloads/release/python-250/

#### 4. PHP

Download and install *PHP 5.4 x86 Non Thread Safe* zip file from http://windows.php.net/download

- You will need to extract the zip file and add the extraction location to your system PATH manually.
- Rename php.ini-development to php.ini and open the file for editing
- Uncomment `;date.timezone =` and set it to [any compatible timezone designation](http://php.net/manual/en/timezones.america.php) such as *America/New_York*
- Uncomment `; extension_dir = "ext"`
- Uncomment the following extensions so that they load
  - php_mbstring.dll
  - php_mysql.dll
  - php_mysqli.dll
  - php_pdo_mysql.dll
  - php_openssl.dll

#### 5. MySQL

Download and run *MySQL Community Edition Installer* from http://dev.mysql.com/downloads/mysql/

*Note: You will need a free Oracle account in order to download MySQL. Only the server is necessary, but some may find the MySQL workbench and other tools useful as well.*


#### 6. Composer

Download the and run the *Windows Installer* from https://getcomposer.org/download/

----------

### Preparing The Dependencies

#### Configuring The Database

Using either MySQL workbench or other MySQL interface, create a new schema/database with utf8mb4 collation. You will need to write down the name, username, and password for later use.

#### Configuring The Project

This project utilizes many automation tools and frameworks to simplify the development process. Start by opening a command line window in your project directory.

- Type `npm install` and hit enter
- Type `npm install -g grunt-cli` and hit enter
- Ensure the command line tool is being run as administrator and type `composer install` and hit enter

For security reasons, parameters.yml is not included in the repository. It will be generated for you when you run the composer install command, and can then be found in */app/config*.

You will be asked for a series of variables during parameter generation. You may hit enter to use the default for all variables other than the database information, which you will need to match to the appropriate details on your machine as configured in the previous section.

Now open a command line window in your project directory

- Type `php app/console doctrine:schema:update --force` and hit enter

Now we need to create the default superadmin user

- Type `php app/console fos:user:create username email@somewhere.com password` (replacing username, email, and password with your own) and hit enter
- Type `php app/console fos:user:promote username ROLE_ADMIN` and hit enter
- Type `php app/console fos:user:promote username ROLE_SUPERADMIN` and hit enter

----------

### Workflow

Now that your system is configured, you are ready to begin development!

Open a command line window in your project directory and type `grunt` and hit enter. This will launch the local web server @ [http://localhost:8000](http://localhost:8000) and begin monitoring the project for changes.

Whenever the files within the project change, the automation from Grunt will kick in so long as the command line window stays open.

----------