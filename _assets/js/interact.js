(function() {
	/* FORTIS CLIENT JS LIBRARY ----------------------------------------------------
	------------------------------------------------------------------------------*/
	window.Fortis = {};

	Fortis.storage = new Burry.Store('web_storage');

	Fortis.fn = {
		storageSpace: function() {
	        var allStrings = '';
	        for(var key in window.localStorage){
	            if(window.localStorage.hasOwnProperty(key)){
	                allStrings += window.localStorage[key];
	            }
	        }
	        return allStrings ? 3 + ((allStrings.length*16)/(8*1024)) : 0;
		},

		urlParam: function(sParam) {
		    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		        sURLVariables = sPageURL.split('&'),
		        sParameterName,
		        i;

		    for (i = 0; i < sURLVariables.length; i++) {
		        sParameterName = sURLVariables[i].split('=');

		        if (sParameterName[0] === sParam) {
		            return sParameterName[1] === undefined ? true : sParameterName[1];
		        }
		    }
		}
	};

	window.Fortis = Fortis;

 	/* HISTORYJS -------------------------------------
 	------------------------------------------------*/
 	$(document).ready(function() {
	 	var timeMachine = {
	 		loadPage:function() {

	 		},

	 		init: function() {
	 			if (!History.enabled) { return false; }

	 			//init history adapter
			 	History.Adapter.bind(window, 'statechange', function() {
			 		var State = History.getState();
					//History.log(State.data, State.title, State.url);
					/*
					$.get(State.url, function(response) {
	            		$('.content .content-inner').html($(response).find('.content .content-inner').html());
	            	});
            		setTimeout(function() {
						$(document).trigger("resize");
						$("body").scrollTop(0);
            		}, 1);
			 		*/
			 	});

			 	//override links
			 	/*
				$("body").on("click", '.content-card a', function(event) {
					event.preventDefault();
					event.stopPropagation();

					var url   = $(this).attr('href');
					var title = $(this).attr('title');

					History.replaceState({data:title}, title, url);
				});
				*/
	 		}
	 	}

	 	timeMachine.init();
 	});

	/* OLD CODE --------------------------------------------------------------------
	------------------------------------------------------------------------------*/
	params = {
		//jquery elements
		navEl: ".navbar",
		pageEl: ".page",
		sideEl: ".app-sidebar",

		//marquee
		currentSlide: 1,
		skipNextMarquee: false,

		//sticky nav
		stickyNavInit: false,

		//sticky editor
		stickyEditorOffset: false,
	};

	/* Functions ----------------------------------
	---------------------------------------------*/
	isMobile = function() {
		var isMobile = false;
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
		    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

		return isMobile;
	};

	enableAnimations = function() {
		if (!isMobile()) {
			$('body').addClass('animate');
		}
	};

	extendPage = function() {
		//makes the page taller so that there is never blank space under the footer
		if (!$(".footer").length || !$(".page").length) { return; }

		$(".page .page-constraint").css("min-height", "20px");
		var difference = $("body")[0].scrollHeight - (($(".footer").offset().top + $(".footer").outerHeight()));
		if (difference > 0) {
			var newHeight = $(".page .page-constraint").outerHeight() + difference;
			$(".page .page-constraint").css("min-height", newHeight);
		}
	};

	shrinkNav = function() {
		//shrink the bar based on screen size
		if ($('.app-menu-icon').is(':visible') || $(window).width() < 800) {
			$(params.navEl).addClass("shrink");
		} else {
			$(params.navEl).removeClass("shrink");
		}
	};

	collapseNav = function() {
		//collapse the bar based on scrolling
		if (!$(params.pageEl).length) { return;}
		var contentOffset = $(params.pageEl).offset().top;
		var scrollTop = $(window).scrollTop();
		if ($('.app-menu-icon').is(':visible') || scrollTop >= (contentOffset - 58)) {
			$(params.navEl).addClass("collapsed");
		} else {
			$(params.navEl).removeClass("collapsed");
		}
	};

	showAppSidebar = function(scrollTop) {

		if (!$(params.sideEl).hasClass('open')) {
			//not opened yet
			$(params.sideEl).addClass('open');
			$('.blackout').addClass('show');
			$('body').css("overflow", "hidden");
			$('body').css("position", "fixed");
			$("body").css("left", "0px");
			$("body").css("right", "0px");

			if (scrollTop) {
				$("body").css("top", scrollTop);
			}
		}
	};

	hideAppSidebar = function() {
		if ($(params.sideEl).hasClass('open')) {
			//opened
			$(params.sideEl).removeClass('open')
			$('.blackout').removeClass('show');
			$('body').css("overflow", "visible");
			$('body').css("position", "relative");
			$("body").css("top", 0);
			$("body").css("left", "auto");
			$("body").css("right", "auto");
		}
	};

	toggleAppSidebar = function(openState) {
		/*
			In order to scroll the sidebar without scrolling
			the page body, we add some smoke and mirrors.

			The page scrolling is disabled, which normally
			returns the user to the top of the page but we
			don't want that.

			What we do is move the entire page up into the
			negatives based on the scroll position. When
			the app menu is closed, we restore the scroll
			position.
		*/
		var scrollTop;
		var rescroll = false;

		if ($(params.sideEl).hasClass('open')) {
			//get the scroll position by calculating the page top offset
			var scrollTop = $("body").css("top").replace(/[^-\d\.]/g, '') * -1;
			rescroll = true;
		} else {
			//get negative offset to be used for 'top' style attribute
			var scrollTop = $(window).scrollTop() * -1;
		}

		if (openState == true) {
			//force open
			showAppSidebar(scrollTop);
		} else if (openState == false) {
			//force close
			hideAppSidebar();
			if (rescroll) { $("body").scrollTop(scrollTop); }
		} else {
			//natural toggle
			if ($(params.sideEl).hasClass('open')) {
				hideAppSidebar();
				if (rescroll) { $("body").scrollTop(scrollTop); }
			} else {
				showAppSidebar();
				$("body").css("top", scrollTop);
			}
		}
	};

	monitorAppSidebar = function() {
		if ($(window).width() > 640) {
			toggleAppSidebar(false)
		}
	};

	marqueePrevious = function(event) {
		var slideCount = $('.marquee .inner .slide').length;
		var index      = $('.pager-btn').index($('.pager-btn.current')) -1;

		if (index < 0) {
			index = slideCount-1;
		}

		$('.pager-btn').eq(index).click();
	};

	marqueeNext = function(event) {
		var slideCount = $('.marquee .inner .slide').length;
		var index      = $('.pager-btn').index($('.pager-btn.current')) +1;

		if (index > (slideCount - 1)) {
			index = 0;
		}

		$('.pager-btn').eq(index).click();
	};

	marqueeUpdateClickAction = function(slide) {
		var el = $('.marquee-click-action');
		var url = $(slide).attr('data-url');
		if (typeof url !== typeof undefined && url !== false) {

			//external or internal?
			var a = new RegExp('/' + window.location.host + '/');
			if(!a.test(url)) {
				//external
				el.attr('target', '_blank');
			} else {
				//internal
				el.attr('target', '_self');
			}

			el.attr('href', url);
			if ($('body').hasClass('animate')) {
				el.fadeIn();
			} else {
				el.css('display','block');
			}
		} else {
			el.attr('href', 'javascript:void(0);');
			if ($('body').hasClass('animate')) {
				el.fadeOut();
			} else {
				el.css('display','none');
			}
		}
	};

	initMarquee = function() {
		var slideCount = $('.marquee .inner .slide').length;
		if (slideCount > 0) {
			//show the first one and init the pager display
			$('.marquee .inner .slide').eq(0).removeClass('hidden');
			$('.marquee .counter').html(params.currentSlide + '/' + slideCount);

			marqueeUpdateClickAction($('.marquee .inner .slide').eq(0));

			//add the buttons and activate the first one
			for (i = 1; i<= slideCount; i++) {
				$('.marquee .pager').append('<div class="pager-btn"></div>');
			}
			$('.marquee .pager-btn').eq(0).addClass('current');

			//register button clicks
			$('.marquee .pager').click(function(event) {
				event.preventDefault();
				event.stopPropagation();
			});
			$('.marquee .pager-btn').click(function(event) {
				event.preventDefault();
				event.stopPropagation();

				var index = $('.marquee .pager-btn').index(this);

				//reconfigure our params
				params.skipNextMarquee = true;
				params.currentSlide = index + 1;

				//update counter
				$('.marquee .counter').html(params.currentSlide + '/' + slideCount);

				//style the buttons
				$('.marquee .pager-btn').removeClass('current');
				$(this).addClass('current');

				//activate the slide
				$('.marquee .inner .slide').addClass('hidden');
				$('.marquee .inner .slide').eq(index).removeClass('hidden');
				marqueeUpdateClickAction($('.marquee .inner .slide').eq(index));
			});

			if (slideCount <= 1) {
				$('.marquee .pager').css('display','none');
				$('.marquee .counter').css('display','none');
			} else {
				//show swipe indicator
				if ('ontouchstart' in window) {
					$('.marquee .swipe-indicator').addClass('show');
					var indicatorTimer = setTimeout(function() {
						$('.marquee .swipe-indicator').removeClass('show');
					}, 4500);
				}

				var timer = setInterval(function() {
					if (params.skipNextMarquee) {
						params.skipNextMarquee = false;
						return;
					}

					var previousSlide = params.currentSlide;
					var currentSlide  = params.currentSlide + 1;

					//reset to start if we're at end
					if (currentSlide > slideCount) {
						currentSlide = 1;
					}

					//swap out classes
					$('.marquee .inner .slide').eq(currentSlide -1).removeClass('hidden');
					$('.marquee .inner .slide').eq(previousSlide -1).addClass('hidden');
					$('.marquee .pager-btn').removeClass('current');
					$('.marquee .pager-btn').eq(currentSlide -1).addClass('current');
					marqueeUpdateClickAction($('.marquee .inner .slide').eq(currentSlide -1));

					//update the pager
					$('.marquee .counter').html(currentSlide + '/' + slideCount);

					//set the new slide
					params.currentSlide = currentSlide;

				}, 6000)
			}
		}
	};

	deterimineRTT = function() {
		if (!$(params.pageEl).length || !$('.return-to-top').length || !$('.page-constraint').length) { return; }
		var contentOffset = $(params.pageEl).offset().top;
		var rightOffset = ($(window).width() - ($('.page-constraint').eq(0).offset().left + $('.page-constraint').eq(0).outerWidth())) + 7;
		if ($(window).scrollTop() >= (contentOffset - 58) && $(window).scrollTop() > 0) {
			$('.return-to-top').css('right', rightOffset + 'px');
			$('.return-to-top').fadeIn();
		} else {
			$('.return-to-top').fadeOut();
		}
	};

	resizeModals = function() {
		$('.modal').removeClass('small tiny full modal-top');
		if ($(window).width() < 641) {
			//small
			$('.modal').addClass('full');
		} else if ($(window).width() < 1024) {
			//medium
			$('.modal').addClass('small modal-top');
		} else {
			//large
			$('.modal').addClass('tiny modal-top');
		}
	};

	loginOverrides = function() {
		if (window.urlParam('show_login') == '1' && ($("#login-register-modal").length)) {
			$(window).load(function() {
				$("#login-register-modal").foundation('open');
			});
		}

		var formButtons = $("#login-register-modal").find("input[type='submit']");

		//add button class to login and register
		formButtons.addClass("primary button");
		formButtons.css('display','block');

		//login ---------------
		$("form[action='/login_check']").submit(function(e) {
			e.preventDefault();
			e.stopPropagation();

			var loginButton  = $(this).find("input[type='submit']");
			var alertCallout = $(this).closest(".tabs-panel").find(".callout");

			alertCallout.removeClass("alert success").addClass("hide");
			loginButton.attr("value", "Logging In...").addClass("disabled");

            $.ajax({
                type        : $(this).attr( 'method' ),
                url         : $(this).attr( 'action' ),
                data        : $(this).serialize(),
                dataType    : "json",
                success: function(data, status, object) {
                	if (!data.success) {
						loginButton.attr("value", "Log In").removeClass("disabled");
						alertCallout.html(data.message).addClass('alert').removeClass("hide");
                	} else {
                		window.location.reload();
                	}
                },
                error: function(data, status, object){
					loginButton.attr("value", "Log In").removeClass("disabled");
                }
            });
		});

		//register ------------
		var firstPassword = $("form[action='/register/']").find("input[name='fos_user_registration_form[plainPassword][first]']");
		var secondPassword = $("form[action='/register/']").find("input[name='fos_user_registration_form[plainPassword][second]']");

		firstPassword.on('change', function() {
			//keep the second password input up to date
			secondPassword.val(firstPassword.val());
		});
		$("form[action='/register/']").submit(function(e) {
			e.preventDefault();
			e.stopPropagation();


			var registerButton = $(this).find("input[type='submit']");
			var alertCallout   = $(this).closest(".tabs-panel").find(".callout");

			alertCallout.removeClass("alert success").addClass("hide");
			registerButton.attr("value", "Registering...").addClass("disabled");

            $.ajax({
                type        : $(this).attr( 'method' ),
                url         : $(this).attr( 'action' ),
                data        : $(this).serialize(),
                dataType    : "json",
                success: function(data, status, object) {
                	if (!data.success) {
                		//data.errors has the details
					    alertCallout.html('');
					    for (var k in data.errors) {
					        if (data.errors.hasOwnProperty(k)) {
					        	alertCallout.append('<div>' + data.errors[k] + '</div>');
					        }
					    }
						alertCallout.addClass("alert").removeClass("hide");
						registerButton.attr("value", "Register").removeClass("disabled");
                	} else {
                		if (data.state == 'confirmed') {
							registerButton.attr("value", "Logging In...");
                			window.location.reload();
                		} else {
							registerButton.attr("value", "Register").removeClass("disabled");
                			alertCallout.html(data.message);
							alertCallout.addClass("success").removeClass("hide");
                		}
                	}
                },
                error: function(data, status, object){
            		alertCallout.html("Something Went Wrong").addClass("alert").removeClass("hide");
					registerButton.attr("value", "Register").removeClass("disabled");
                }
            });
		});

		//resetting -----------
		$("form[action='/resetting/send-email']").submit(function(e) {
			e.preventDefault();
			e.stopPropagation();

			var resetButton  = $(this).find("input[type='submit']");
			var alertCallout = $(this).closest(".tabs-panel").find(".callout");

			alertCallout.removeClass("alert success").addClass("hide");
			resetButton.attr("value", "Resetting...").addClass("disabled");

            $.ajax({
                type        : $(this).attr( 'method' ),
                url         : $(this).attr( 'action' ),
                data        : $(this).serialize(),
                dataType    : "json",
                success: function(data, status, object) {
                	if (!data.success) {
						resetButton.attr("value", "Reset Password").removeClass("disabled");
						alertCallout.html(data.message).addClass('alert').removeClass("hide");
                	} else {
						resetButton.attr("value", "Reset Password").removeClass("disabled");
						alertCallout.html(data.message).addClass('success').removeClass("hide");
                	}
                },
                error: function(data, status, object){
					resetButton.attr("value", "Reset Password").removeClass("disabled");
					alertCallout.html("Something Went Wrong").addClass('alert').removeClass("hide")
                }
            });
		});
	};

	window.urlParam = function(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};


	stickyWYSIWYG = function() {
		var el = $('#toolbar.ql-snow');
		var container = $('#editor.ql-container');
		if (el.length > 0) {
			//el.addClass('sticky');
			var scrollTop = $(window).scrollTop();
			
			if (!params.stickyEditorOffset) {
				params.stickyEditorOffset = el.offset().top;
			}


			//should it be sticky?
			if (scrollTop >= (params.stickyEditorOffset - el.outerHeight())) {
				el.addClass('shadow sticky');
				container.css('margin-top', el.outerHeight() + 'px');

				var rightOffset = ($(window).width() - (container.offset().left + container.outerWidth()));
				var leftOffset  = (container.offset().left);
				el.css('left',  leftOffset  + 'px');
				el.css('right', rightOffset + 'px');
				el.css('top', $('.navbar').outerHeight() + 'px');
			} else {
				el.removeClass('shadow sticky');
				container.css('margin-top', '0px');
				el.css('left',  'inherit');
				el.css('right', 'inherit');
				el.css('top', 'inherit');
			}

			/*
			//collapse the bar based on scrolling
			if (!$(params.pageEl).length) { return;}
			var contentOffset = $(params.pageEl).offset().top;
			var scrollTop = $(window).scrollTop();
			if ($('.app-menu-icon').is(':visible') || scrollTop >= (contentOffset - 58)) {
				$(params.navEl).addClass("collapsed");
			} else {
				$(params.navEl).removeClass("collapsed");
			}
			*/
		}
	}

	window.showContentCards = function(reverse) {

		var showCard = setInterval(function() {
			if ($('.content-card.hidden').length > 0) {
				if (reverse) {
					$('.content-card.hidden').last().removeClass('hidden');
				} else {
					$('.content-card.hidden').first().removeClass('hidden');
				}
			} else {
				clearInterval(showCard);
			}
		}, 100);
	}

	window.infiniteScroll = function() {
		if (window.infinite_scroll && $("#load-more-pager").length > 0 && !$('#load-more-pager').hasClass("disabled")) {
			var fromBottom = ($(window).scrollTop() + $(window).height() - $(document).height()) * -1;
			if (fromBottom <= 400) {
				$("#load-more-pager").trigger('click');
			}
		}
	}

	/* Extend jQuery ------------------------------
	---------------------------------------------*/
	$.fn.scrollStopped = function(callback) {
	  var that = this, $this = $(that);
	  $this.scroll(function(ev) {
	    clearTimeout($this.data('scrollTimeout'));
	    $this.data('scrollTimeout', setTimeout(callback.bind(that), 250, ev));
	  });
	};

	/* Events -------------------------------------
	---------------------------------------------*/
	$(document).ready(function() {
		window.showContentCards();
		window.infiniteScroll();
		shrinkNav();
		collapseNav();

		extendPage();
		monitorAppSidebar();
		resizeModals();

		loginOverrides();

		$("span.timeago").timeago();
		$(document).foundation();
		initMarquee();

		enableAnimations();
		deterimineRTT();
		stickyWYSIWYG();

	    $('body').on('click', '#toggle-sidebar', function() {
	    	toggleAppSidebar();
	    });

	    $('body').on('click', '.blackout', function() {
	    	toggleAppSidebar(false);
	    });

	    if (isMobile()) {
		    $('body').on('swipeleft', function(event) {
		    	toggleAppSidebar(false);
		    });
		    $('body').on('swiperight', function(event) {

		    	var leftBoundary = $( window ).width() * 0.08;
				if ( event.swipestart.coords[0] < leftBoundary && $(window).width() <= 640) {
					toggleAppSidebar(true);
				}
		    });
		}

	    $('.marquee').on('swipeleft', function(e) {
	    	marqueeNext(e);
	    });

	    $('.marquee').on('swiperight', function(e) {
	    	marqueePrevious(e);
	    });

	    $('.return-to-top').on('click', function(e) {
	    	if ($('body').hasClass('animate')) {
		    	$("body").animate({ scrollTop: 0 }, 200);
		    } else {
	        	$('body').scrollTop(0);
		    }
	    });
	});

	$(window).on('resize', function(){
		//window.infiniteScroll();
		shrinkNav();
		collapseNav();
		extendPage();
		deterimineRTT();
		monitorAppSidebar();
		resizeModals();
		stickyWYSIWYG();
	});

	$(window).scroll(function() {
		collapseNav();
		deterimineRTT();
		stickyWYSIWYG();
		window.infiniteScroll();
	});

	$(document).on('DOMMouseScroll mousewheel', '.scroll-lock', function(ev) {
	    var $this = $(this),
        scrollTop = this.scrollTop,
        scrollHeight = this.scrollHeight,
        height = $this.height(),
        delta = (ev.type == 'DOMMouseScroll' ?
            ev.originalEvent.detail * -40 :
            ev.originalEvent.wheelDelta),
        up = delta > 0;

	    var prevent = function() {
	        ev.stopPropagation();
	        ev.preventDefault();
	        ev.returnValue = false;
	        return false;
	    }

	    if (!up && -delta > scrollHeight - height - scrollTop) {
	        // Scrolling down, but this will take us past the bottom.
	        $this.scrollTop(scrollHeight);
	        return prevent();
	    } else if (up && delta > scrollTop) {
	        // Scrolling up, but this will take us past the top.
	        $this.scrollTop(0);
	        return prevent();
	    }
	});

	/* HistoryJS API ------------------------------
	---------------------------------------------*/
    // Bind to StateChange Event
    $(document).ready(function() {
    	//var History = window.History;
    	/*
	    History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
	        var State = History.getState(); // Note: We are using History.getState() instead of event.state
	    });
		*/
    });

})();