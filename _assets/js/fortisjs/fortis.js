/* WINDOW UTILITIES ----------------------
----------------------------------------*/

(function() {
	window.debounce = function(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};

	window.throttle = function(fn, threshhold, scope) {
		threshhold || (threshhold = 250);
		var last, deferTimer;
		return function () {
			var context = scope || this;

			var now = +new Date,
			args = arguments;
			if (last && now < last + threshhold) {
				// hold on to it
				clearTimeout(deferTimer);
				deferTimer = setTimeout(function () {
					last = now;
					fn.apply(context, args);
				}, threshhold);
			} else {
				last = now;
				fn.apply(context, args);
			}
		};
	}
})();

/* SPECIAL EVENTS ------------------------
----------------------------------------*/

(function() {

	// IMAGE BACKFILL ----------------------------------------------------------------------------

	$(document).on('image-backfill', function() {
		var search = '*[data-img]';
		$(search).each(function() {
			if ($(this).attr('data-img') != "") {
				$(this).backstretch($(this).attr('data-img'));
				$(this).removeAttr('data-img');
			}
		});
	});

})();

/* FORTIS CLIENT JS ----------------------
----------------------------------------*/

(function() {

	Fortis = window.Fortis || {};

	Fortis.params = {
		unix_build_date: null,
		lastID: null,
		pageRoot: '/',
		previousScroll: 0,
		isNavOffscreen: false,
		scrollMonitor: []
	};

	Fortis.fn = {
		debounce : function(func, wait, immediate) {
			var timeout;
			return function() {
				var context = this, args = arguments;
				var later = function() {
					timeout = null;
					if (!immediate) func.apply(context, args);
				};
				var callNow = immediate && !timeout;
				clearTimeout(timeout);
				timeout = setTimeout(later, wait);
				if (callNow) func.apply(context, args);
			};
		},
		storageSpace: function() {
	        var allStrings = '';
	        for(var key in window.localStorage){
	            if(window.localStorage.hasOwnProperty(key)){
	                allStrings += window.localStorage[key];
	            }
	        }
	        return allStrings ? 3 + ((allStrings.length*16)/(8*1024)) : 0;
		},

		urlParam: function(sParam, urlString) {
			urlString = urlString || decodeURIComponent(window.location.search.substring(1));

			urlStringChunks = urlString.split('?');
			if (urlStringChunks.length > 1) {
				urlString = urlStringChunks[1];
			}

		    var sPageURL = urlString,
		        sURLVariables = sPageURL.split('&'),
		        sParameterName,
		        i;

		    for (i = 0; i < sURLVariables.length; i++) {
		        sParameterName = sURLVariables[i].split('=');

		        if (sParameterName[0] === sParam) {
		            return sParameterName[1] === undefined ? true : sParameterName[1];
		        }
		    }
		},

		showContentCards: function(reverse) {
			$('.content-card.hidden').removeClass('hidden');
			/*
			var showCard = setInterval(function() {
				if ($('.content-card.hidden').length > 0) {
					if (reverse) {
						$('.content-card.hidden').last().removeClass('hidden');
					} else {
						$('.content-card.hidden').first().removeClass('hidden');
					}
				} else {
					clearInterval(showCard);
				}
			}, 100);
			*/
		},

		determineRTT: function() {
			var contentOffset = 150;
			if ($('.full-screen-canvas').hasClass('open')) {
				//tracking the overlay
				currentScroll = $('.full-screen-canvas').scrollTop();
				$('.return-to-top .label').hide();
			} else {
				//tracking the window
				currentScroll = $(window).scrollTop();

				if (History.enabled && Fortis.fn.urlParam('view') !== 'flat') {
					$('.return-to-top .label').show();
				}
			}

			if (currentScroll >= (contentOffset) && currentScroll > 0) {
				$('.return-to-top').addClass('shown');
			} else {
				$('.return-to-top').removeClass('shown');
			}
		},

		registerInteractions: function() {
			//init our return to top if we've loaded into a specific page
			if (parseInt(Fortis.fn.urlParam('page')) && parseInt(Fortis.fn.urlParam('page')) > 1 && History.enabled && Fortis.fn.urlParam('view') !== 'flat') {
				$('.return-to-top .label .text').html(Fortis.fn.urlParam('page') + ' of ' + $('.return-to-top .label').attr('data-total-pages'));
				$('.return-to-top').addClass('shown');
			} else if (!History.enabled || Fortis.fn.urlParam('view') == 'flat') {
				$('.return-to-top .label').addClass('disabled');
			}

			$('body').on('click', '.return-to-top', function(event) {
				if ($('.full-screen-canvas').hasClass('open')) {
					//tracking the overlay
					el = '.full-screen-canvas';
				} else {
					//tracking the window
					el = 'body, html';
				}


				//are we returning to page 1 of a card hub, or just scrolling up the page?
				if ($('.cardset').first().attr('data-fortis-page') && parseInt($('.cardset').first().attr('data-fortis-page')) !== 1 && !$('.full-screen-canvas').hasClass('open')) {
					//we need to return to page 1
					$('.return-to-top .icon i').hide();
					$('.return-to-top .load-indicator').show();
					window.location.replace(Fortis.params.pageRoot);
				} else {
					//we can just scroll up the page
					$(el).animate({ scrollTop: 0 }, 400);
				}
			});

			$('body').on('click', '.return-to-top .new-content-label', function(event) {
				event.stopPropagation();
				event.preventDefault();
				window.location.replace(Fortis.params.pageRoot);
			});

			$('body').on('click', '.return-to-top .label', function(event) {
				event.stopPropagation();
				event.preventDefault();
			});

			$('body').on('click', "a[href='#refresh']", function(event) {
				event.stopPropagation();
				event.preventDefault();
				window.location.reload();
			});

			$('body').on('click','.soundcloud-play', function(event) {
				event.preventDefault();
				event.stopPropagation();

				Fortis.parser.playSoundCloud($(this));
			});

			$('body').on('click', '#popout-player', function(event) {
				event.preventDefault();
				event.stopPropagation();

				var trackID = $(this).attr('data-soundcloud-id');

				window.open("/popoutplayer/" + trackID, "FortisPopoutPlayer", "width=380, height=380");
			});
		},

		applyDataImg: function(el,fade) {
			el = $(el);

			var doParse = function(el, fade) {
				el = $(el);
				var imgUrl = el.attr('data-img');
				if (imgUrl && imgUrl !== '') {
					if (!fade || Fortis.fn.isMobile()) {
						el.backstretch(imgUrl);
					} else {
						el.backstretch(imgUrl,{fade:750});
					}
				}
			};

			if (document.readyState == "complete") {
				doParse(el, fade);
			} else {
				$(window).load(function() {
					doParse(el, fade);
				});
			}
		},

		showCanvas: function(scrollTop) {
			$('.full-screen-canvas .load-indicator').addClass('shown');
			if (!$('.full-screen-canvas').hasClass('open')) {
				var scrollTop = $(window).scrollTop();
				//not opened yet

				$('.full-screen-canvas').css('z-index','1000');
				$('.full-screen-canvas').addClass('open');

				var oldWidth = $('body').width();
				$('body').css("overflow", "hidden");
				$('body').css("position", "fixed");
				//$('body').css("margin-right",'15px');
				$("body").css("left", "0px");
				$("body").css("right", "0px");

				var newWidth = $('body').width();
				$('body').css("margin-right",(newWidth - oldWidth) + 'px');
				$('.navbar').css("margin-right",(newWidth - oldWidth) + 'px');

				$("body").css("top", scrollTop * -1);
				$('.full-screen-canvas').scrollTop(0);
				$('.navbar').addClass('collapsed');
				$('.navbar-bg').addClass('opaque');
			}
		},

		hideCanvas: function() {
			if ($('.full-screen-canvas').hasClass('open')) {
				if (window.ga) {
					//we have google analytics, so lets send the page view
					//set the tracker to the new page
					ga('set', {
						page: window.location.pathname,
						title: document.title,
					});

					//send a page view
					ga('send','pageview', '', {
						hitCallback: function() {
							return;
						}
					});
				}
				var scrollTop = $("body").css("top").replace(/[^-\d\.]/g, '') * -1;
				$('.full-screen-canvas').removeClass('open');

				//opened
				setTimeout(function() {
					$('.full-screen-canvas').removeClass('clicked');
					$('.full-screen-canvas').css('z-index','-1000');
					$('.canvas-page').html('');
				},300);

				$('body').attr('style','');
				$('.navbar').attr('style','');
				/*
				$('body').css("overflow", "visible");
				$('body').css("position", "relative");
				$('body').css("margin-right",'0px');
				$("body").css("top", '0');
				$("body").css("left", "auto");
				$("body").css("right", "auto");
				*/
				if ($(window).scrollTop() < 100) {
					$('.navbar').removeClass('collapsed');
				}
				$('.full-screen-canvas .canvas-page').removeClass('shown');
				$(window).scrollTop(scrollTop);
				setTimeout(function() {
					$(window).trigger('resize');
				},300);
			}
		},

		showAppSidebar: function(scrollTop) {
			if (!$('.app-sidebar').hasClass('open')) {
				//not opened yet
				$('.material-icon').addClass('arrow');
				$('.app-sidebar').addClass('open');
				$('.full-screen-blocker').show();
				if (!$('.full-screen-canvas').hasClass('open')) {
					$('body').css("overflow", "hidden");
					$('body').css("position", "fixed");
					$("body").css("left", "0px");
					$("body").css("right", "0px");

					if (scrollTop) {
						$("body").css("top", scrollTop);
					}
				}
				//setTimeout(function() {
					if ($('.navbar').hasClass('offscreen')) {
						$('.navbar').removeClass('offscreen');
						Fortis.params.isNavOffscreen = true;
					}
				//},250);
			}
		},

		hideAppSidebar: function() {
			if ($('.app-sidebar').hasClass('open')) {
				//opened
				$('.material-icon').removeClass('arrow');
				$('.app-sidebar').removeClass('open')
				$('.full-screen-blocker').hide();
				if (!$('.full-screen-canvas').hasClass('open')) {
					$('body').css("overflow", "visible");
					$('body').css("position", "relative");
					$("body").css("top", 0);
					$("body").css("left", "auto");
					$("body").css("right", "auto");
				}
			}
		},

		toggleAppSidebar: function(openState) {
			var scrollTop;
			var rescroll = false;

			if ($('.app-sidebar').hasClass('open')) {
				//get the scroll position by calculating the page top offset
				var scrollTop = $("body").css("top").replace(/[^-\d\.]/g, '') * -1;
				rescroll = true;
			} else {
				//get negative offset to be used for 'top' style attribute
				var scrollTop = $(window).scrollTop() * -1;
			}

			if (openState == true) {
				//force open
				Fortis.fn.showAppSidebar(scrollTop);
			} else if (openState == false) {
				//force close
				Fortis.fn.hideAppSidebar();
				if (rescroll) {
					$(window).scrollTop(scrollTop);
				}
			} else {
				//natural toggle
				if ($('.app-sidebar').hasClass('open')) {
					Fortis.fn.hideAppSidebar();
					if (rescroll) {
						$(window).scrollTop(scrollTop);
					}
				} else {
					Fortis.fn.showAppSidebar();
					$("body").css("top", scrollTop);
				}
			}
		},

		isMobile: function() {
			var isMobile = false;
			if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
			    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

			if (isMobile) {
				$('body').addClass('no-animate');
			}
			return isMobile;
		},

		registerAppMenu: function() {
			$('body').on('click', "a[href='#app-menu']", function(event) {
				event.preventDefault();
				event.stopPropagation();

				Fortis.fn.toggleAppSidebar();
			});

		    $('body').on('click', '.full-screen-blocker', function() {
		    	Fortis.fn.toggleAppSidebar(false);
		    });

		    if (Fortis.fn.isMobile()) {
			    $('body').on('swipeleft', function(event) {
			    	Fortis.fn.toggleAppSidebar(false);
			    });

		    	$('body').on('swiperight', function(event) {
		    		var leftBoundary = $( window ).width() * 0.08;
					if ( event.swipestart.coords[0] < leftBoundary && $('.navbar .page-constraint #app-menu-toggle').first().is(':visible')) {
						Fortis.fn.toggleAppSidebar(true);
					}
		    	});
		    }
		},

		isElementInViewport: function(el) {
		    //special bonus for those using jQuery
		    if (typeof jQuery === "function" && el instanceof jQuery) {
		        el = el[0];
		    }

		    var rect = el.getBoundingClientRect();

		    return (
		        rect.top >= 0 &&
		        rect.left >= 0 &&
		        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
		        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
		    );
		},

		registerHistory: function() {
			if (History.enabled && Fortis.config.useCanvasPages) {
		 		//initialize adapter
				History.Adapter.bind(window, 'statechange', function() {
				 	var State = History.getState();
				 	if (State.data.data == "canvas") {
				 		$('.canvas-page').html('loading...');
						$.get(State.url, function(response) {
		            		$('.canvas-page').html($(response).find('.content-constraint').html());
		            		Fortis.parser.parseArticle('.canvas-page');
		            		Fortis.parser.datetime($('.canvas-page span.timeago'), 1);
		            		$('.full-screen-canvas .load-indicator').removeClass('shown');
							$('.full-screen-canvas .canvas-page').addClass('shown');
		            	});
		        	}

		        	if (State.data.expect && $(State.data.expect).length <= 0) {
			        	window.location.reload();
		        	}

		        	if (State.data.canvas && State.data.canvas == "false") {
		        		Fortis.fn.hideCanvas();
		        		//$('.canvas-page .content-area').remove();
		        		//window.DISQUS = null;
		        		//$("iframe[src*='disqus.com/home/preload']").remove();

		        		//$(window).trigger('resize');
		        	} else if (State.data.canvas && State.data.canvas == "true") {
		        		Fortis.fn.showCanvas();
		        	}
				});

				//configure
				if ('scrollRestoration' in history) {
					// Back off, browser, I got this...
					history.scrollRestoration = 'manual';
				}

				//setup events
				$('body').on('click', 'a.card-action', function(event) {
					event.preventDefault();
					event.stopPropagation();
					var that = $(this);

					if ($(this).closest('.content-card').length > 0) {
						//this is a card action
						if ($(this).closest('.content-card').attr('data-fortis-page')) {
							var url   = that.attr('href');
							var title = that.attr('data-title');
							var id    = that.closest('.content-card').attr('data-fortis-id');

							History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot + "?page=" + $(this).closest('.content-card').attr('data-fortis-page') + "&id=" + id + "&t=" + Date.now());
							History.pushState({data:'canvas', canvas: 'true', fromUrl: Fortis.params.pageRoot }, title, url);

							if (window.ga) {
								//we have google analytics, so lets send the page view
								//set the tracker to the new page
								ga('set', 'page', url);
								ga('set', 'title', title);

								//send a page view
								ga('send','pageview', url, {
									hitCallback: function() {
									}
								});
							}
						}
					} else {
						//this is not a card action
						var url = $(this).attr('href');
						var title = $(this).attr('data-title');

						History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot);
						History.pushState({data:'canvas', canvas: 'true', fromUrl: Fortis.params.pageRoot }, title, url);

						if (window.ga) {
							//we have google analytics, so lets send the page view
							//set the tracker to the new page
							ga('set', 'page', url);
							ga('set', 'title', title);

							//send a page view
							ga('send','pageview', url, {
								hitCallback: function() {
								}
							});
						}
					}
				});

				$('body').on('click', '.full-screen-canvas', function(event) {
					if ($(event.target).hasClass('full-screen-canvas') && $(event.target).css('z-index') > -1000 && !$(event.target).hasClass('clicked')) {
						$('.full-screen-canvas').addClass('clicked');
						History.back();
					}
				});
			} else if (History.enabled) {
				//history enabled, but useCanvasPages disabled
				$('body').on('click', 'a.card-action', function(event) {
					var that = $(this);
					if ($(this).closest('.content-card').attr('data-fortis-page')) {
						var url   = that.attr('href');
						var title = that.attr('data-title');
						var id    = that.closest('.content-card').attr('data-fortis-id');

						History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot + "?page=" + $(this).closest('.content-card').attr('data-fortis-page') + "&id=" + id + "&t=" + Date.now());
					}
				});
			} else {
				//no history
				$('body').on('click', '.full-screen-canvas', function(event) {
					if ($(event.target).hasClass('full-screen-canvas') && !$(event.target).hasClass('clicked')) {
						Fortis.fn.hideCanvas();
					}
				});
			}
		},

		imageReelPrev: function(el) {
			var reel = $(el).parent().find('.widget-image-reel');
			var activating = parseInt(reel.attr('data-active-img')) - 1;
			var image = reel.find('li').eq(activating);
			var counter = reel.parent().find('.image-page-indicator .counter');

			var childCount = reel.find('li').length;

			var speed=250;
			if (activating < 0) {
				activating = childCount-1;
				image = reel.find('li').eq(childCount-1);
				return;
			}

			var width  = image.width();
			var offset = reel.find('li').eq(activating).position().left;
			var left   = reel.scrollLeft();
			offset = offset + left;

			reel.animate({scrollLeft:offset},speed, 'linear');

			reel.attr('data-active-img', activating);

			if (activating == 0) {
				reel.parent().find('.go-left').addClass('disabled');
			} else {
				reel.parent().find('.go-left').removeClass('disabled');
			}

			reel.parent().find('.go-right').removeClass('disabled');
			counter.html((activating+1) + ' / ' + (childCount));
		},

		imageReelNext: function(el) {
			var reel = $(el).parent().find('.widget-image-reel');
			var activating = parseInt(reel.attr('data-active-img')) + 1;
			var image = reel.find('li').eq(activating);
			var counter = reel.parent().find('.image-page-indicator .counter');

			var childCount = reel.find('li').length;

			var speed = 250;
			if (activating > childCount-1) {
				activating = 0;
				image = reel.find('li').eq(0);
				return;
			}

			var width  = image.width();
			var offset = reel.find('li').eq(activating).position().left;
			var left   = reel.scrollLeft();
			offset = offset + left;

			reel.animate({scrollLeft:offset},speed, 'linear');
			reel.attr('data-active-img', activating);

			if (activating == childCount -1) {
				reel.parent().find('.go-right').addClass('disabled');
			} else {
				reel.parent().find('.go-right').removeClass('disabled');
			}

			reel.parent().find('.go-left').removeClass('disabled');
			counter.html((activating+1) + ' / ' + (childCount));
		},
		registerImageReels: function() {
			//todo - move this to it's own widget js
			$('body').on('click','.widget-image-reel-container .go-right', function() {
				Fortis.fn.imageReelNext($(this));
			});

			$('body').on('click','.widget-image-reel-container .go-left', function() {
				Fortis.fn.imageReelPrev($(this));
			});

		    if (Fortis.fn.isMobile()) {
			    $('body').on('swipeleft', '.widget-image-reel-inner', function(event) {
					//Fortis.fn.imageReelNext($(this).find('.go-right'));
					event.stopPropagation();
					event.preventDefault();
			    });

		    	$('body').on('swiperight', '.widget-image-reel-inner', function(event) {
					//Fortis.fn.imageReelPrev($(this).find('.go-left'));
					event.stopPropagation();
					event.preventDefault();
		    	});

			    $('body').on('swipeleft', '.table-constraint', function(event) {
					//Fortis.fn.imageReelNext($(this).find('.go-right'));
					event.stopPropagation();
					event.preventDefault();
			    });

		    	$('body').on('swiperight', '.table-constraint', function(event) {
					//Fortis.fn.imageReelPrev($(this).find('.go-left'));
					event.stopPropagation();
					event.preventDefault();
		    	});
		    }

			$(window).on('resize', function(){
				var that = $(this);
				$('.widget-image-reel').each(function() {
					if ($(this).parent().parent().hasClass('mobile-view')) { return; }
					var active = parseInt($(this).attr('data-active-img'));
					var offset = $(this).find('li').eq(active).position().left;
					var left   = $(this).scrollLeft();
					offset = offset + left;
					$(this).animate({scrollLeft:offset},0);
				});
			});
		},

		determineStickyNav:function() {
			if (Fortis.fn.isMobile() && !navigator.userAgent.match(/(iPad)/) && Fortis.config.allowCollapseNavInMobile) {
				var currentScroll;
				//are we tracking overlay page or are we tracking the window?
				if ($('.full-screen-canvas').hasClass('open')) {
					//tracking the overlay
					currentScroll = $('.full-screen-canvas').scrollTop();
				} else {
					//tracking the window
					currentScroll = $(window).scrollTop();
				}
				var navHeight = -1 * ($('.navbar').height());

				if (currentScroll == Fortis.params.previousScroll) {
					//nothing changed
				} else if (currentScroll < Fortis.params.previousScroll) {
					//scrolling up
					$('.navbar').removeClass('offscreen');
					$('.navbar-bg').removeClass('offscreen');
				} else {
					//scrolling down
					$('.navbar').addClass('offscreen');
					$('.navbar-bg').addClass('offscreen');
				}
				Fortis.params.previousScroll = currentScroll;
			}
		}
	};

	Fortis.handleNavSizing = function() {
		/*
		if ($(window).width() > 640) {
			Fortis.fn.hideAppSidebar();
		}
		*/

		if ($(window).scrollTop() >= 100) {
			$('.navbar').addClass('collapsed');
		} else {
			if (!$('.app-sidebar').hasClass('open') && !$('.full-screen-canvas').hasClass('open')) {
				$('.navbar').removeClass('collapsed');
			}
		}

		/*
		var fadeLimiter = 70;
		var totalAmount = $('.main-panel').outerHeight() - fadeLimiter;
		var visibleAmount = totalAmount - $(window).scrollTop();

		if ($(window).width() > 640) {
			Fortis.fn.hideAppSidebar();
		}

		if (!$('.app-sidebar').hasClass('open') && !$('.full-screen-canvas').hasClass('open')) {
			//$('.main-panel-spacer').css('height', + $('.main-panel').outerHeight() + 'px');
			if (visibleAmount + fadeLimiter > 0) {
				//page offsets and parallax
				//$('.main-panel').css('top', 1 * $(window).scrollTop() / 3.8 + 'px');

				//opacity
				var percentVisible = visibleAmount / totalAmount;
				var percentHidden = 1 - percentVisible;
				$('.navbar-bg').css('background-color','rgba(30,33,36,' + percentHidden +')');
				//$('.main-panel .greyout').css('opacity', percentHidden);
				//$('.main-panel .cta').css('opacity', percentVisible);
			}
		}

		if ($(window).scrollTop() >= 100) {
			$('.navbar').addClass('sticky');
		} else {
			if (!$('.app-sidebar').hasClass('open') && !$('.full-screen-canvas').hasClass('open')) {
				$('.navbar').removeClass('sticky');
			}
		}

		if ($(window).scrollTop() >= $('.main-panel').outerHeight() - fadeLimiter) {
			$('.navbar-bg').addClass('opaque');
		} else {
			if (!$('.app-sidebar').hasClass('open') && !$('.full-screen-canvas').hasClass('open')) {
				$('.navbar-bg').removeClass('opaque');
			}
		}
		*/
	};

	/* Events -------------------------------------
	---------------------------------------------*/
	$(document).on('firstCardLoad', function() {
		setTimeout(function() {
			var id = Fortis.params.lastID;
			var el = $(".content-card[data-fortis-id='" + id + "']");
			if (id && !$('full-screen-canvas').hasClass('open') && el.length > 0) {
				var offset = ($(".content-card[data-fortis-id='" + id + "']").offset().top - 90);
				window.scrollTo(0,offset);
			}
			//$(Fortis.infiniteScroller.config['newer'].btn).trigger('click');
		},250);
	});

	Fortis.init = function(unix_build_date) {
		//parameter setup
		Fortis.params.lastID = Fortis.fn.urlParam('id');
		Fortis.params.unix_build_date = unix_build_date;

		if (Fortis.fn.urlParam('history') == 'no') {
			History.enabled = false;
		}

		$(document).ready(function() {
			//History.enabled = false;

			if (History.enabled && Fortis.fn.urlParam('view') !== 'flat') {
				$('.load-more-container').show();
				//$('.load-less-container').show();
				//$('.load-newer-container').show();
			} else {
				$('.stock-pager').show();
			}

			Fortis.handleNavSizing();
			Fortis.fn.determineStickyNav();
			Fortis.fn.showContentCards();
			Fortis.fn.determineRTT();
			Fortis.fn.registerInteractions();
			Fortis.fn.registerAppMenu();
			Fortis.fn.registerHistory();
			Fortis.fn.registerImageReels();
			$(document).foundation();
			Fortis.parser.datetime($('.content-headers').find('span.timeago'), true);

			//window scroll monitoring
			var lastScrollTop = 0;
			var scrollFn = throttle(function() {
				var direction, st = $(window).scrollTop();
				if (st > lastScrollTop) {
					direction = "down";
				} else {
					direction = "up";
				}

				lastScrollTop = st;

				$.each(Fortis.params.scrollMonitor, function(key, func) {
					func(direction);
				});
			}, 500);
			$(window).scroll(scrollFn);

			//add items to the scroll monitor
			Fortis.params.scrollMonitor.push(
				Fortis.handleNavSizing,
				Fortis.fn.determineRTT,
				Fortis.fn.determineStickyNav
			);

			var overlayScrollFn = throttle(function() {
				Fortis.fn.determineRTT();
				Fortis.fn.determineStickyNav();
			}, 250);
			$('.full-screen-canvas').scroll(overlayScrollFn);

			//if we have a page loaded, let's scroll to it
			var page = parseInt(Fortis.fn.urlParam('page'));
			if (page > 1 && !Fortis.params.lastID && !$('full-screen-canvas').hasClass('open')) {
				var el     = $(".content-card[data-fortis-page='" + page + "']").first();
				var offset = ($(el).offset().top - 90);
				window.scrollTo(0,offset);
			}

			console.log('Fortis Client Runtime Loaded');

			$(window).on('resize', function(){
				Fortis.handleNavSizing();
				Fortis.fn.determineRTT();
			});

			$(window).load(function() {
				$(document).trigger('image-backfill');
			});

			$(document).on('DOMMouseScroll mousewheel', '.scroll-lock', function(ev) {
			    var $this = $(this),
		        scrollTop = this.scrollTop,
		        scrollHeight = this.scrollHeight,
		        height = $this.height(),
		        delta = (ev.type == 'DOMMouseScroll' ?
		            ev.originalEvent.detail * -40 :
		            ev.originalEvent.wheelDelta),
		        up = delta > 0;

			    var prevent = function() {
			        ev.stopPropagation();
			        ev.preventDefault();
			        ev.returnValue = false;
			        return false;
			    }

			    if (!up && -delta > scrollHeight - height - scrollTop) {
			        // Scrolling down, but this will take us past the bottom.
			        $this.scrollTop(scrollHeight);
			        return prevent();
			    } else if (up && delta > scrollTop) {
			        // Scrolling up, but this will take us past the top.
			        $this.scrollTop(0);
			        return prevent();
			    }
			});
		});
	}

})();

