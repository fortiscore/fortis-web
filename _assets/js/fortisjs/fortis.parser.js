/* ENSURE LOAD ---------------------------
----------------------------------------*/
$.fn.extend({
    ensureLoad: function(handler) {
        return this.each(function() {
            if(this.complete) {
                handler.call(this);
            } else {
                $(this).load(handler);
            }
        });
    }
});


/* FORTIS Parameters JS ------------------
----------------------------------------*/
(function() {
	Fortis = window.Fortis || {};

	Fortis.parser = {
		params: {
			width: 560,
			height: 315
		},

		datetime: function(targetEl, full) {
			el = $(targetEl);
			var shortFormat = 'MMM D';
			var fullFormat  = 'MMMM Do YYYY, h:mma';

			if (full) {
				var format = fullFormat;
				var theMoment = window.moment(el.attr('datetime'));
				var formatted = theMoment.format(format);
			} else {
				var format = shortFormat;
				var theMoment = window.moment(el.attr('datetime'));
				var formatted = theMoment.format(format).toUpperCase();
			}

			el.html(formatted);
		},

		modifyImageReels: function(el) {
			if (Fortis.fn.isMobile() && 'ontouchstart' in window) {
				$('.widget-image-reel-container').each(function() {
					$(this).addClass('mobile-view');
				});
			}
		},

		embedImageReel: function(el) {
			el   = $(el);
			that = this;

			$('p', el).each(function() {
				//does this paragraph only contain images?
				//lets check the textContent first, and then go through the children
				var textContent = $(this).text().replace(/ /g,'').replace(/\r?\n/g, '');
				if (textContent == '') {
					//does this paragraph only have images as children?
					var containsOnlyImageGroup = true;

					if ($(this).children().length > 1) {
						$(this).children().each(function() {
							if ($(this).prop("tagName") !== "IMG") {
								//this paragraph has more than images in it
								containsOnlyImageGroup = false;
								return false;
							} else {
								$(this).wrap('<li><a href="' + $(this).attr('src') + '" target="_blank"></a></li>');
								$(this).before('<span class="helper" style="display:inline-block; height:100%; vertical-align:middle;"></span>');
							}
						});
					} else {
						containsOnlyImageGroup = false;
					}

					if (containsOnlyImageGroup) {
						//this paragraph has only images and is ready to be parsed
						childCount = $(this).find('img').length;
						$(this).addClass('widget-image-reel no-select').attr('data-active-img','0');
						$(this).wrap('<div class="widget-image-reel-container card-shadow"><div class="widget-image-reel-inner"></div></div>');
						$(this).parent().append('<div class="go-left disabled no-select"><div class="indicator"><i class="fi-play"></i></div></div><div class="go-right no-select"><div class="indicator"><i class="fi-play"></i></div></div>');
						$(this).parent().append('<div class="image-page-indicator"><div class="counter card-shadow">1 / ' + (childCount) + '</div></div>');
					}
				}
			});
		},

		embedTwitter: function(el) {
			//todo - this is pretty much broken. look into oembed / embed alternatives
			el = $(el);
			that = this;

			$('a[href*="twitter.com/"]', el).each(function () {
				if ($(this).attr('title') !== 'embed' && $(this).attr('title') !== 'embed+') { $(this).attr('title',''); return; }
	            var url     = $(this).attr('href');
                var tweetID = url.substring(url.lastIndexOf('/status/') + 8);
                var media = 'hidden';
                if ($(this).attr('title') == 'embed+') {
                	media = "visible";
                }

	            //var formattedURL = 'http://twitframe.com/show?url=' + url;

	            $(this).replaceWith('<div class="tweet" data-show-media="' + media + '" data-tweet-id="' + tweetID + '"></div>');
	            /*
                $(this).replaceWith('\
                	<div class="iframe-constraint mini"><div class="iframe-wrapper"><iframe class="embed-twitter" src="' + formattedURL + '" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe></div></div>\
                ');
                */
			});
		},

		parseTwitter: function(el) {
			var doParse = function(el) {
				$('.tweet', el).each(function() {
					var id = $(this).attr("data-tweet-id");
					var showMedia = $(this).attr("data-show-media");

					twttr.widgets.createTweet(id, $(this)[0], {
						conversation : 'none',    // or all
						cards        : showMedia,  // or visible 
						linkColor    : '#55ACEE', // default is blue
						theme        : 'light'    // or dark
					}).then (function (el) {
						$(el).addClass('tweet-embed-iframe');
						var head = $(el).contents().find('head');
						if (head.length) {
							head.append('<style>.EmbeddedTweet { cursor: default !important; max-width: 100% !important; width: 100% !important; } .timeline .stream { max-width: none !important; width: 100% !important; }</style>');
							$(el).contents().find('.EmbeddedTweet').attr('data-click-to-open-target','');
						}
						$('el').append($('<div class=timeline>'));
      					//el.contentDocument.querySelector(".footer").style.display = "none";
      				});
				});
			}

			if (document.readyState == "complete") {
				doParse(el);
			} else {
				window.onload = (function(){
					doParse(el);
				});
			}
		},

		playSoundCloud: function(el) {
			el = $(el);
			var src = $(el).attr('data-src');
			$(el).replaceWith('<iframe class="embed-soundcloud" src="' + src + '" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe>')
		},

		embedSoundcloud: function(el) {
			el = $(el);
			that = this;

            $('a[href*="api.soundcloud.com/"]', el).each(function () {
				if ($(this).attr('title') !== 'embed') { $(this).attr('title',''); return; }
	            var url     = $(this).attr('href');
	            var trackID = url.substring(url.lastIndexOf('/tracks/') + 8);

	            /*
                $(this).replaceWith('\
                	<div class="iframe-constraint mini"><div class="iframe-wrapper"><iframe class="embed-soundcloud" src="" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe></div></div>\
                ');
                */
                $(this).replaceWith('\
                	<div class="iframe-constraint mini"><div class="iframe-wrapper">\
                		<a class="button soundcloud-play" data-src="https://w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/' + trackID + '&auto_play=true&hide_related=true&show_comments=false&show_user=true&show_reposts=false&visual=true"><center><i class="fi-play"></i> Play Audio</center></a>\
                	</div></div>\
                ');
			});

			//todo add support for timestamps
		},

		embedTwitch: function(el) {
			el = $(el);
			that = this;

			/* twitch parameters
				Either Required
					channel : Channel name for live streams. (ex. twitch)
					video : Video ID for past broadcasts. Has no effect if channel is specified. (ex. v123456)

				Optional
					muted : Sets the initial muted state. true or false. Defaults to false.
					autoplay : Automatically starts playing without the user clicking play. true or false. Defaults to true.
					time : Start playback at the given timestamp for videos ONLY. Must be in the format 1h2m3s specifying hours, minutes, and seconds respectively. Defaults to the start of the video.
			*/
	                
	        //Embed Twitch
            $('a[href*="twitch.tv/"]', el).each(function () {
            	if ($(this).attr('title') !== 'embed') { $(this).attr('title',''); return; }
                var url     = $(this).attr('href');
                var channel = url.substring(url.lastIndexOf('/') + 1);

                $(this).replaceWith('\
                	<div class="iframe-constraint"><div class="iframe-wrapper"><iframe class="embed-twitch" src="http://player.twitch.tv/?channel=' + channel + '&muted=false&autoplay=false" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe></div></div>\
                ');
            });

            //todo - allow past broadcasts
		},

		embedYoutube: function(el) {
			el = $(el);
			that = this;

			/* youtube parameters
				autohide
					Value 0: The player controls are always visible.
					Value 1: The player controls hides automatically when the video plays.
					Value 2 (default): If the player has 16:9 or 4:3 ratio, same as 1, otherwise same as 0.

				autoplay
					Value 0 (default): The video will not play automatically when the player loads.
					Value 1: The video will play automatically when the player loads.

				controls
					Value 0: Player controls does not display. The video loads immediately.
					Value 1 (default): Player controls display. The video loads immediately.
					Value 2: Player controls display, but the video does not load before the user initiates playback. 

				loop
					Value 0 (default): The video will play only once.
					Value 1: The video will loop (forever).

				playlist
					A comma separated list of videos to play (in addition to the original URL).
			*/
	                
	        //Embed Youtube	                
            $('a[href*="youtube.com/watch"]', el).each(function () {
            	if ($(this).attr('title') !== 'embed') { $(this).attr('title',''); return; }
                var url     = $(this).attr('href');
                var videoID = Fortis.fn.urlParam('v', url);
                var t       = Fortis.fn.urlParam('t',url);
                var start   = Fortis.fn.urlParam('start',url);
                if (t) {
                	timeCode = t;
                } else if (start) {
                	timeCode = start;
                } else {
                	timeCode = 0;
                }

                $(this).replaceWith('\
                	<div class="iframe-constraint"><div class="iframe-wrapper"><iframe class="embed-youtube" src="http://www.youtube.com/embed/' + videoID + '?autohide=1&autoplay=0&controls=2&loop=0&start=' + timeCode + '" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe></div></div>\
                ');
            });

            $('a[href*="youtu.be"]', el).each(function () {
            	if ($(this).attr('title') !== 'embed') { $(this).attr('title',''); return; }
                var url     = $(this).attr('href');
                var videoID = url.substring(url.lastIndexOf('/') + 1).split('?')[0].split('&')[0];                
                var t       = Fortis.fn.urlParam('t', url);
                var start   = Fortis.fn.urlParam('start', url);
                if (t) {
                	timeCode = t;
                } else if (start) {
                	timeCode = start;
                } else {
                	timeCode = 0;
                }

                $(this).replaceWith('\
                	<div class="iframe-constraint"><div class="iframe-wrapper"><iframe class="embed-youtube" src="http://www.youtube.com/embed/' + videoID + '?autohide=1&autoplay=0&controls=2&loop=0&start=' + timeCode + '" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe></div></div>\
                ');
            });
		},

		embedMedia: function(el) {
			//does all embedding listed above
			el = $(el);
			this.embedImageReel(el);
			this.modifyImageReels(el);
			this.embedYoutube(el);
			this.embedTwitch(el);
			this.embedSoundcloud(el);
			this.embedTwitter(el);
			this.parseTwitter(el);
		},

		linksToNewPage: function(el) {
			var that = this;
			//ensure all links point to a new page
			$('a', el).each(function() {
				if ($(this)[0].host !== window.location.host) {
					$(this).attr('target','_blank');
				}
			});
		},

		floatImages: function(el) {
			$('img', el).each(function() {
				//grab the details
				var target          = $(this);
				var title           = $(this).attr('title');
				
				if (!title) { return; }

				var isFloatingLeft   = title.lastIndexOf('fig-left', 0) === 0;
				var isFloatingRight  = title.lastIndexOf('fig-right', 0) === 0;
				var isFloatingCenter = title.lastIndexOf('fig-center', 0) === 0;

				//is it isolated?
				/*
				if ((isFloatingLeft || isFloatingRight) && $(this).is(":only-child") && $(this).parent().text().length == 0) {
					//$(this).parent().addClass('clearfix');
					target = $(this).parent();
				}
				*/

				//lets apply our styles
				if (isFloatingLeft) {
					title = title.substring(title.lastIndexOf('fig-left') + 9);
					$(this).wrap('<div class="fig-container floating float-left"><div class="outer card-shadow"><a href="' + $(this).attr('src') + '" target="blank"></a></div></div>');
					$(this).after('<span class="fig"><i class="fi-magnifying-glass"></i> ' + title + '</span>');
					$(this).attr('title','');
				} else if (isFloatingRight) {
					title = title.substring(title.lastIndexOf('fig-right') + 10);
					$(this).wrap('<div class="fig-container floating float-right"><div class="outer card-shadow"><a href="' + $(this).attr('src') + '" target="blank"></a></div></div>');
					$(this).after('<span class="fig"><i class="fi-magnifying-glass"></i> ' + title + '</span>');
					$(this).attr('title','');
				} else if (isFloatingCenter) {
					title = title.substring(title.lastIndexOf('fig-center') + 11);
					$(this).wrap('<div class="fig-container floating float-center"><div class="outer card-shadow"><a href="' + $(this).attr('src') + '" target="blank"></a></div></div>');
					$(this).after('<span class="fig"><i class="fi-magnifying-glass"></i> ' + title + '</span>');
					$(this).attr('title','');
					$(this).closest('p').css('text-align','center');
				}

				$(this).ensureLoad(function() {
					//we now need to set the span to max at this width
					if ($(this).closest('.floating').hasClass('float-left') || $(this).closest('.floating').hasClass('float-right') || $(this).closest('.floating').hasClass('float-center') && $(this).closest('.floating').find('.fig').length > 0) {
						$(this).closest('.floating').find('.fig').css('max-width', $(this).width() + 'px');
					}
				});
				/*
				$(this).load(function() {
					//we now need to set the span to max at this width
					if ($(this).parent().hasClass('float-left') || $(this).parent().hasClass('float-right') && $(this).parent().find('.fig').length > 0) {
						$(this).parent().find('.fig').css('max-width', $(this).width() + 'px');
					}
				});
				*/
			});
		},

		centerImages: function(el) {
			//this will center all images that are by themselves on a line
			el = $(el);

			$('img', el).each(function() {
				if ($(this).is(":only-child") && $(this).parent().text().length == 0) {
					$(this).parent().css('text-align','center');
					$(this).parent().css('clear','both');
				}
			});
		},
		
		underflowTables: function(el) {
			$('table', el).each(function() {
				$(this).wrap('<div class="table-constraint"></div>');
			});
		},

		clearfix: function(el) {
			//this will ensure floated images do not overlap the bottom of an article ever
			el = $(el);
			el.find('p').last().addClass('clearfix');
		},

		parseArticle: function(el) {
			el = $(el);
			this.linksToNewPage(el);
			this.floatImages(el);
			this.centerImages(el);
			this.underflowTables(el);
			this.clearfix(el);
			this.embedMedia(el);
		}
	};

    /*
    //Embed Twitch
  	//setup ignoreFilter array
	var ignoreFilter = new Array();
	ignoreFilter.push('%2Fp%2F','%2Fuser%2Flegal','%2Fadvertise','blog.','secure.','dev.','help.');
	$('a[href*="twitch.tv%2F"]').each(function () {
        //Set our counter
       	var twitchCounter = 0;
        
	   	//Get the channel name and link
		var fullLink = $(this).attr('href');
    
        //Check our ignoreFilter for links we don't want to embed
		var ignoreLink = false;
		ignoreFilter.forEach(function(entry) {
	    	if (fullLink.indexOf(entry) > -1) {
				ignoreLink = true;
			}
		});
		if(!ignoreLink) {
            var linkEnd = fullLink.substring(fullLink.indexOf(".tv%2F") + 6);
   			//seperate the end of the link so that we can parse video id, username, and timecode 
            if (fullLink.indexOf("%3Ft%3D") > -1) {
                //yes there is a timecode
                var timeCode = fullLink.substring(fullLink.indexOf("%3Ft%3D") + 7);
				var linkEnd = fullLink.substring(fullLink.indexOf(".tv%2F") + 6,fullLink.indexOf("%3Ft%3D"));
                var twitchTime = convertTwitchTimeToSeconds(timeCode);
            } else {
                //no time code - not altering
                var twitchTime = 0;
            	var linkEnd = fullLink.substring(fullLink.indexOf(".tv%2F") + 6);
            }
            
   			//now we need to see if there is a video id to know if we should display that video or the live stream
            var subLink = linkEnd.match(/(%2F[a-zA-Z]%2F)/);
    		if (subLink) {
                subLink = subLink[1];
   				//it's a sublink of the channel, so we know it isn't live
				var userName = linkEnd.substring(0,linkEnd.indexOf(subLink));
                	//Remove any trailing slashes from the username
                	var index = userName.indexOf('%2F');
					if(index != -1) { userName = userName.substr(0,index); }
                
				var videoID = linkEnd.substring(linkEnd.indexOf(subLink) + subLink.length);
                if (subLink == '%2Fc%2F') {
                    //this is a video from a chapter
                    var videoEmbedID = ('chapter_id=' + videoID);
                } else {
                    var videoEmbedID = ('archive_id=' + videoID);
                }
				if (userName != '') {
              		//<object bgcolor='#000000' data='http://www.twitch.tv/widgets/archive_embed_player.swf' height='378' id='clip_embed_player_flash' type='application/x-shockwave-flash' width='620'><param name='movie' value='http://www.twitch.tv/widgets/archive_embed_player.                     swf'><param name='allowScriptAccess' value='always'><param name='allowNetworking' value='all'><param name='allowFullScreen' value='true'><param name='flashvars' value='auto_play=false&amp;channel=omieshomies&amp;chapter_id=2681947&amp;title=%2523380%2BNoScope%2Bturn%2Bon%2Bsome%2BSkrillex&amp;start_volume=25'></object><br><a href="http://www.twitch.tv/omieshomies" class="trk" style="padding:2px 0px 4px; display:block; width: 320px; font-weight:normal; font-size:10px; text-decoration:underline; text-align:center;">Watch live video from omieshomies on TwitchTV</a>
    				$(this).before('<div id="FEKembedTwitch" style="width:' + width + 'px;"><b>' + userName + '</b> on Twitch.tv&nbsp;&nbsp;&nbsp;<a href="http://www.twitch.tv/' + userName + '/profile/pastBroadcasts">View Other Videos</a><a href="http://twitch.tv/' + userName + '">View Channel</a><br/>');
                    $(this).after('<span id="FEKembedTwitchSubText">Original Video Link: <a href="' + fullLink + '">Click Here</a></span></div>');
   					$(this).replaceWith('<object bgcolor="#000000" data="http://www.twitch.tv/widgets/archive_embed_player.swf" height="' + height + '" id="clip_embed_player_flash" type="application/x-shockwave-flash" width="' + width + '"><param name="movie" value="http://www.twitch.tv/widgets/archive_embed_player.swf"><param name="allowScriptAccess" value="always"><param name="allowNetworking" value="all"><param name="allowFullScreen" value="true"><param name="flashvars" value="' + videoEmbedID + '&amp;start_volume=25&amp;initial_time=' + twitchTime + '&amp;channel=' + userName + '&amp;auto_play=false"></object>');
				}
			} else {
   				//it's a link to a live video
				//let's also add links to the user's channel and popout chat!
				var userName = linkEnd;
                	//Remove any trailing slashes from the username
                	var index = userName.indexOf('%2F');
					if(index != -1) { userName = userName.substr(0,index); }
				if (userName != '') {
					twitchCounter = twitchCounter + 1;
                    var injectCounter = twitchCounter;
                    $(this).before('<div id="FEKembedTwitch" style="width:' + width + 'px;"><b>' + userName + '</b> on Twitch.tv&nbsp;&nbsp;&nbsp;<a href="#" title="' + userName + '" onClick="var newwindow=window.open(\'http://www.twitch.tv/chat/embed?channel=' + userName + '&popout_chat=true\',\'Twitch.tv Chat Popout\',\'height=400,width=400\'); if (window.focus) {newwindow.focus()} return false;' + '" style="cursor:pointer;">Join Chat</a><a href="http://www.twitch.tv/' + userName + '/profile/pastBroadcasts">View Other Videos</a><a href="http://twitch.tv/' + userName + '">View Channel</a><br/>');
					$(this).replaceWith('<object type="application/x-shockwave-flash" width="' + width + '" height="' + height + '" width="100%" id="live_embed_player_flash" data="http://www.twitch.tv/widgets/live_embed_player.swf?channel=' + userName + '" bgcolor="#000000"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="allowNetworking" value="all" /><param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf" /><param name="flashvars" value="hostname=www.twitch.tv&channel=' + userName + '&auto_play=false&start_volume=25&" /></object></div>');
				}
			}
		}
	});
	*/

})();