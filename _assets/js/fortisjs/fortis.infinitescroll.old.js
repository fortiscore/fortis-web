(function() {
	"use strict";

	//get the fortis library
	Fortis = window.Fortis || {};

	//set up parameters
	Fortis.infiniteScroller = {
		cache: null,

		config: {
			//internal configuration
			cardset:  '.cardset',

			newer: {
				btn: '#load-newer-pager',
				distance: 100,
				prompt:   false,
				enabled:  true,
				running:  false,

				endpoint: function(id) {
					var endpoint = '/command/html/newer_than_id/{id}/{filter}?stamp=' + $.now();
					return endpoint.replace("{id}",id).replace("{filter}", Fortis.infiniteScroller.params.filter);
				}
			},

			older: {
				btn: '#load-more-pager',
				distance: 100,
				prompt:   false,
				enabled:  true,
				running:  false,

				endpoint: function(id) {
					var endpoint = '/command/html/older_than_id/{id}/{filter}?stamp=' + $.now();
					return endpoint.replace("{id}",id).replace("{filter}", Fortis.infiniteScroller.params.filter);
				}
			},
		},

		params: {
			//user provided parameters
			//todo provide defaults and then merge the params in from the init call
		},

		shared: {
			//cache control -----------------------------------------------------------
			initCache: function() {
				window.Burry.isSupported = function() { return false; } //todo broke this on purpose to test network stuff
			 	if (Burry.isSupported()) {
			 		//setup our cache storage and flush expired entries
		 			Fortis.infiniteScroller.cache = new Burry.Store('ajax', 5);
		 			Fortis.infiniteScroller.cache.flushExpired();

		 			//if we have more than 100 entries, flush the cache
				 	var cacheSize = Fortis.infiniteScroller.cache.keys().length;
				 	if (cacheSize >= 100) {
			 			Fortis.infiniteScroller.cache.flush();
			 		}

			 		//if build date doesn't match, flush the cashe and update the build date
			 		if (Fortis.infiniteScroller.shared.getCache('ubd') !== Fortis.params.unix_build_date) {
						Fortis.infiniteScroller.cache.flush();
						Fortis.infiniteScroller.shared.setCache('ubd', Fortis.params.unix_build_date);
			 		}
			 	}
			},

			getCache: function(endpoint) {
		 		if (!Burry.isSupported()) { return false; }
		 		return Fortis.infiniteScroller.cache.get(endpoint);
			},

			setCache: function(endpoint, data) {
		 		if (!Burry.isSupported()) { return false; }
		 		Fortis.infiniteScroller.cache.set(endpoint, data);
			},

			//handling ajax provided data ---------------------------------------------
			fetch: function(cfg) {
				//setup our endpoint
				var id;
				if (cfg == 'newer') {
					id = $(Fortis.infiniteScroller.config.cardset).first().attr('data-fortis-id');
				} else {
					id = $(Fortis.infiniteScroller.config.cardset).last().attr('data-fortis-id');
				}
				var endpoint = Fortis.infiniteScroller.config[cfg].endpoint(id);

				//tell the library we're running a command and disable the button
				Fortis.infiniteScroller.shared.disableBtn(cfg);
				Fortis.infiniteScroller.config[cfg].running = true;

				//obtain the next set of cards
				if (Fortis.infiniteScroller.shared.getCache(endpoint)) {
					//found the set in cache
					Fortis.infiniteScroller.shared.handleSuccess(cfg, Fortis.infiniteScroller.shared.getCache(endpoint), null, null);
				} else {
					//fetch the set from database
					$.ajax({
		                type        : "GET",
		                url         : endpoint,
		                data        : {},
		                dataType    : "json",
		                success: function(data, status, object) {
		                	if (!data.results) {
		                		Fortis.infiniteScroller.shared.handleFailure(cfg, data, status, object);
		                	} else {
		                		Fortis.infiniteScroller.shared.setCache(endpoint, data);
								Fortis.infiniteScroller.shared.handleSuccess(cfg, data, status, object);
		                	}
		                },
		                error: function(data, status, object){
							Fortis.infiniteScroller.shared.handleFailure(cfg, data, status, object);
		                }
		            });
				}
				
				//automatically load without clicking from now on if the option is enabled	
				if ((cfg == "newer" && Fortis.config.autoLoadNewAfterFirst) || (cfg == "older" && Fortis.config.autoLoadOldAfterFirst)) {
					Fortis.infiniteScroller.config[cfg].prompt = false;
				}
			},

			handleSuccess: function(cfg, data, status, object) {
				var oldHeight, oldScroll, oldOffset, i, hasNewContent = false, isReversed = false;
				
				Waypoint.disableAll();
				if (data.total_returned > 0) {

					if (parseInt(data.total_content_count) > Fortis.infiniteScroller.params.total_content_count) {
						//we have new content added to the top
						Fortis.infiniteScroller.params.new_content_count = parseInt(data.total_content_count) - Fortis.infiniteScroller.params.total_content_count;
						if (Fortis.infiniteScroller.params.new_content_count == 1) {
							var storyText = 'Story';
						} else {
							var storyText = 'Stories';
						}
						var theHtml = '<i class="fi-arrow-up"></i> ' + Fortis.infiniteScroller.params.new_content_count + ' New ' + storyText;
						$('.load-newer-container a.loader').html(theHtml).addClass('new');
						$('.return-to-top .label .new-content-label').html(theHtml).removeClass('hidden');
						hasNewContent = true;
					}

					if (cfg == 'newer') {
						//handling newer content loading at the top

						//since we load newer at the top, we need to record our scroll positions
						oldHeight = $(Fortis.infiniteScroller.params.container).outerHeight();
						var reScroll = true;
						oldScroll = $(document).scrollTop();

						//populate the cards into the page
						for (i = 0; i < data.results.length; i++) {
							$(Fortis.infiniteScroller.params.container).prepend(data.results[i]);
						}

						//populate filler into the page if needed
						if (cfg == "newer" && data.total_returned < 12) {
							//newer content is not a full page, so we need to fill it to prevent issues
							for (i=0; i < (12 - data.total_returned); i++) {
								Fortis.infiniteScroller.shared.addFiller('prepend');
							}
						}

						//set cards to be revealed in reverse
						isReversed = true;
					} else {
						oldHeight = $(Fortis.infiniteScroller.params.container).outerHeight();
						oldScroll = $(document).scrollTop();
						oldOffset = $(".cardset").last().offset().top;
						//handling older content loading at the bot
						//update page counter

						var oldVPOffset = oldOffset - oldScroll;
						
						//Fortis.infiniteScroller.params.page = (parseInt($('.cardset').last().attr('data-fortis-page')) + 1);

						//populate the cards into the page
						for (i = 0; i < data.results.length; i++) {
							$(Fortis.infiniteScroller.params.container).append(data.results[i]);
						}
					}

					//prepare and reveal the cards
					Fortis.infiniteScroller.shared.prepareCards(cfg);
					Fortis.fn.showContentCards(isReversed);

					//add disqus comment counts
					if (window.DISQUSWIDGETS) {
						window.DISQUSWIDGETS.getCount();
					}

					//did we need to restore scroll position?
					if (oldHeight && reScroll) {
						var newHeight = $(Fortis.infiniteScroller.params.container).outerHeight();
						window.scrollTo($(window).scrollLeft(), ($(document).scrollTop() + (newHeight - oldHeight)));
					}

					//destroy older cards
					var firstPage = parseInt( $('.cardset').first().attr('data-fortis-page') );
					var lastPage  = parseInt( $('.cardset').last().attr('data-fortis-page')  );
					var pageCount = lastPage - firstPage + 1;
					var extraPages = pageCount - Fortis.config.cardPagesInMemory;

					if (extraPages > 0) {
						//we need to purge some
						if (cfg == 'newer') {
							//newer, so delete from bot
							var cardPage = lastPage;
							for (i = lastPage; extraPages > 0; i--) {
								$(".content-card[data-fortis-page='" + i + "']").remove();
								extraPages = extraPages - 1;
							}

							//update our pager values
							Fortis.infiniteScroller.params.oldestLoadedPage = parseInt($('.cardset').last().attr('data-fortis-page'));
							Fortis.infiniteScroller.config['older'].enabled = true;

							//enable the older button
							Fortis.infiniteScroller.shared.enableBtn('older');

						} else {
							//older, so delete from top
							var cardPage = lastPage;
							for (i = firstPage; extraPages > 0; i++) {
								$(".content-card[data-fortis-page='" + i + "']").remove();
								extraPages = extraPages - 1;
							}

							//update our pager values
							Fortis.infiniteScroller.params.newestLoadedPage = parseInt($('.cardset').first().attr('data-fortis-page'));
							Fortis.infiniteScroller.config['newer'].enabled = true;

							//calculate new offsets so that our scroll position in the page doesnt change
							if ($(".cardset[data-fortis-page='" + (lastPage-1) + "']").length > 0) {
								var newOffset = $(".cardset[data-fortis-page='" + (lastPage-1) + "']").last().offset().top;
								var newScroll = $(document).scrollTop();
								var newVPOffset = Math.round(newOffset - newScroll);
								var difference = oldVPOffset - newVPOffset;
								var offset = newScroll - difference;

								//scroll it
								$('body, html').animate({ scrollTop: offset }, 0);
							}

							//enable the newer button
							Fortis.infiniteScroller.shared.enableBtn('newer');
						}
					}
				}


				var total_pages         = $('.return-to-top .label').attr('data-total-pages');
				var remaining_new_pages = parseInt($('.cardset').first().attr('data-fortis-page')) - 1;
				var remaining_old_pages = total_pages - parseInt($('.cardset').first().attr('data-fortis-page')) - 1;

				if (remaining_new_pages < 1 && !hasNewContent) {
					var oldScroll = $(window).scrollTop();
					var oldHeight = $(document).height();
					
					$('.load-newer-container').hide();
					
					var newHeight = $(document).height();
					var offset    = $(document).scrollTop() - (oldHeight - newHeight);
					$('body, html').animate({ scrollTop: offset }, 0);
				} else if (remaining_new_pages < 1) {
					//has new content
					$('.load-newer-container').show();
					$('.load-newer-container a.button').show();
				}

				setTimeout(function() {
					Waypoint.enableAll();
					Fortis.infiniteScroller.config.locked = false;
				},500);

				//is this the end of content?
				if (data.EOF || (cfg == "newer" && remaining_new_pages < 1) || (cfg == "older" && remaining_old_pages < 1)) {
					Fortis.infiniteScroller.shared.finishBtn(cfg);
					Fortis.infiniteScroller.config[cfg].running = false;
					Fortis.infiniteScroller.config[cfg].enabled = false;
				} else {
					Fortis.infiniteScroller.shared.enableBtn(cfg);
					Fortis.infiniteScroller.config[cfg].running = false;
					setTimeout(function() {
						Fortis.infiniteScroller.shared.autoLoad(cfg);
					}, 1);
				}
			},

			handleFailure: function(cfg, data, status, object) {
				//no html data could be obtained
				Fortis.infiniteScroller.shared.enableBtn(cfg);
				Fortis.infiniteScroller.config[cfg].running = false;
				Fortis.infiniteScroller.config.locked = false;
				Fortis.infiniteScroller.shared.autoLoad(cfg);
			},

			//ui events ---------------------------------------------------------------
			addFiller: function(mode, el) {
				//get the first page
				var page, theHtml;
				if (mode == 'replace') {
					page    = parseInt($(el).attr('data-fortis-page'));
					theHtml = '<div class="medium-4 columns panel content-card card-filler cardset tighten" data-fortis-page="' + page + '" data-fortis-prepped="1"><a href="#refresh"><i class="fi-alert"></i><br />Some Content Was Removed. Click Here To Refresh The List.</a></div>';
					$(el).after(theHtml);
					$(el).remove();
				} else if (mode == 'prepend') {
					page    = parseInt($('.cardset[data-fortis-prepped]').first().attr('data-fortis-page')) - 1;
					theHtml = '<div class="medium-4 columns panel content-card card-filler tighten" data-fortis-page="' + page + '" data-fortis-prepped="1"><a href="#refresh"><i class="fi-alert"></i><br />Some Content Was Removed. Click Here To Refresh The List.</a></div>';
					$(Fortis.infiniteScroller.params.container).prepend(theHtml);
				} else if (mode == 'append') {
					$(Fortis.infiniteScroller.params.container).append(theHtml);
				}
			},

			finishBtn: function(cfg) {
				if (cfg == 'newer') {
					$(Fortis.infiniteScroller.config[cfg].btn).addClass('disabled').html(Fortis.infiniteScroller.params.newFinishLabel);
					$('#load-newer-pager').hide();
				} else {
					$(Fortis.infiniteScroller.config[cfg].btn).addClass('disabled').html(Fortis.infiniteScroller.params.oldFinishLabel);
				}

				return $(Fortis.infiniteScroller.config[cfg].btn);
			},

			enableBtn: function(cfg) {
				var label = (cfg == 'newer' ? Fortis.infiniteScroller.params.newLabel : Fortis.infiniteScroller.params.oldLabel);
				$(Fortis.infiniteScroller.config[cfg].btn).removeClass('disabled').addClass('card-shadow').html(label);

				if (cfg == 'newer') {

					var oldHeight = $(document).height();
					var oldScroll = $(document).scrollTop();

					$('#load-newer-pager').show();
					$('.load-newer-container').show();

					var newHeight = $(document).height();
					window.scrollTo($(window).scrollLeft(), ($(document).scrollTop() + (newHeight - oldHeight)));
				}

				return $(Fortis.infiniteScroller.config[cfg].btn);
			},

			disableBtn: function(cfg) {
				var label = (cfg == 'newer' ? Fortis.infiniteScroller.params.newLoadingLabel : Fortis.infiniteScroller.params.oldLoadingLabel);
				$(Fortis.infiniteScroller.config[cfg].btn).addClass('disabled').removeClass('card-shadow').html(label);
				return $(Fortis.infiniteScroller.config[cfg].btn);
			},

			isInTopRange: function(cfg) {
				var fromTop = $(window).scrollTop() - $('#article-entry-point').offset().top;
				if (fromTop <= Fortis.infiniteScroller.config[cfg].distance && !$('.full-screen-canvas').hasClass('open')) {
					return true;
				} else {
					return false;
				}
			},

			isInBottomRange: function(cfg) {
				var winBottom = ($(window).height());
				var elBottom  = $('#article-entry-point').offset().top + $('#article-entry-point').outerHeight();
				var fromWinBottom = $(document).height() - ($(window).scrollTop() + $(window).height());
				var space = $(document).height() - elBottom;
				var fromBottom = fromWinBottom - space;

				if (fromBottom <= Fortis.infiniteScroller.config[cfg].distance && !$('.full-screen-canvas').hasClass('open')) {
					return true;
				} else {
					return false;
				}
			},

			autoLoad: function(cfg) {
				if (cfg == 'newer') {
					if (!Fortis.infiniteScroller.config[cfg].prompt) {
						//are we near the beginning page? (but not both ranges)
						if (Fortis.infiniteScroller.shared.isInTopRange(cfg)) {
							$(Fortis.infiniteScroller.config[cfg].btn).trigger('click');
						}
					}
				} else {
					if (!Fortis.infiniteScroller.config[cfg].prompt) {
						//are we near the end of the page? (but not both ranges)
						if (Fortis.infiniteScroller.shared.isInBottomRange(cfg)) {
							$(Fortis.infiniteScroller.config[cfg].btn).trigger('click');
						}
					}
				}
			},

			//init routines -----------------------------------------------------------
			registerEvents: function(cfg) {
				//button press todo
				$("body").on("click", Fortis.infiniteScroller.config[cfg].btn, function() {
					//dont run in the following conditions
					if (Fortis.infiniteScroller.config.locked || $(this).hasClass('disabled') || $('body').css("overflow") == 'hidden' || !$(Fortis.infiniteScroller.config[cfg].btn).is(":visible")) { return; }
					Fortis.infiniteScroller.config.locked = true;

					//prepare to fetch
					if (!Fortis.infiniteScroller.config[cfg].running) {
						Fortis.infiniteScroller.shared.fetch(cfg);
					}
				});

				//window resize
				$(window).on('resize', function(){
					Fortis.infiniteScroller.shared.autoLoad(cfg);
				});

				//window scroll
				var scrollFn = function(direction) {
					if (direction == "up") {
						Fortis.infiniteScroller.shared.autoLoad('newer');
					} else {
						Fortis.infiniteScroller.shared.autoLoad('older');
					}
				};
				Fortis.params.scrollMonitor.push(scrollFn);

				//automatically load newer content
				if (cfg == 'newer') {
					Fortis.infiniteScroller.shared.autoLoad(cfg);
				}
			},

			prepareCards: function(cfg) {
				if (cfg !=='first' && !Fortis.infiniteScroller.config[cfg].enabled) { return; }

				var page = parseInt(Fortis.infiniteScroller.params.page);

				if (cfg == "newer") {
					if (!Fortis.infiniteScroller.params.newestLoadedPage) {
						Fortis.infiniteScroller.params.newestLoadedPage = page -1;
					} else {
						Fortis.infiniteScroller.params.newestLoadedPage = Fortis.infiniteScroller.params.newestLoadedPage -1;
					}
					page = Fortis.infiniteScroller.params.newestLoadedPage;
				} else if (cfg == "older") {
					if (!Fortis.infiniteScroller.params.oldestLoadedPage) {
						Fortis.infiniteScroller.params.oldestLoadedPage = page +1;
					} else {
						Fortis.infiniteScroller.params.oldestLoadedPage = Fortis.infiniteScroller.params.oldestLoadedPage +1;
					}
					page = Fortis.infiniteScroller.params.oldestLoadedPage;
				}

				if (page < 1) { page = 1; }

				if (cfg == 'first') {
					//trigger custom first card loaded event
					$.event.trigger({
						type: 'firstCardLoad'
					});
				}

				//prepare each card
				$(Fortis.infiniteScroller.config.cardset + ":not([data-fortis-prepped])").each(function() {
					//append information and run tasks
					Fortis.parser.datetime($(this).find(".timeago"));
					$(this).attr('data-fortis-page', page);
					Fortis.fn.applyDataImg($(this).find('.image-cover'),true);
				});

				//set waypoints if we have history support
				if (History.enabled) {
					//first card
					$(Fortis.infiniteScroller.config.cardset + ":not([data-fortis-prepped])").first().waypoint(function(direction) {
						if (!$('.full-screen-canvas').hasClass("open") && !Fortis.returntotop) {
							if ($(this.element).attr('data-fortis-page') == '1') {
								History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot);
							} else {
								History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot + "?page=" + $(this.element).attr('data-fortis-page'));
							}
							$('.return-to-top .label').attr('data-current-page',$(this.element).attr('data-fortis-page')).find('.text').html($(this.element).attr('data-fortis-page') + " of " + $('.return-to-top .label').attr('data-total-pages'));
						}
					}, {offset:'25%'});

					//last card
					$(Fortis.infiniteScroller.config.cardset + ":not([data-fortis-prepped])").last().waypoint(function(direction) {
						if (!$('.full-screen-canvas').hasClass("open") && !Fortis.returntotop) {
							if ($(this.element).attr('data-fortis-page') == '1') {
								History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot);
							} else {
								History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot + "?page=" + $(this.element).attr('data-fortis-page'));
								}
							$('.return-to-top .label').attr('data-current-page',$(this.element).attr('data-fortis-page')).find('.text').html($(this.element).attr('data-fortis-page') + " of " + $('.return-to-top .label').attr('data-total-pages'));
						}
					}, {offset:'25%'});
				}

				//mark the cards as prepped
				$(Fortis.infiniteScroller.config.cardset + ":not([data-fortis-prepped])").attr('data-fortis-prepped', 1);

				//reflow content by triggering window resize
				$(window).trigger('resize');
			}
		},

		init: function(params) {
			//load in our parameters and update global Fortis params
			this.params                     = params;
			Fortis.params.pageRoot          = this.params.pageRoot;
			Fortis.infiniteScroller.params.new_content_count = 0;
			Fortis.infiniteScroller.config.locked = false;

			if (Fortis.fn.isMobile()) {
				Fortis.infiniteScroller.config.newer.distance = 100;
				Fortis.infiniteScroller.config.older.distance = 100;
			}

			//initialize our cache
			this.shared.initCache();

			//setup events for newer and older
			this.shared.registerEvents('older');
			this.shared.registerEvents('newer');
			
			//prepare cards
			this.shared.prepareCards('first');
		}
	};
})();