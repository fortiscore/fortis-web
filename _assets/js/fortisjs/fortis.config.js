/* FORTIS Parameters JS ------------------
----------------------------------------*/
(function() {
	Fortis = window.Fortis || {};

	Fortis.config = {
		useCanvasPages: true,

		autoLoadNewAfterFirst: true,
		autoLoadOldAfterFirst: true,
		cardPagesInMemory:2,

		allowCollapseNavInMobile: true, //disabled for ipad
	};

})();