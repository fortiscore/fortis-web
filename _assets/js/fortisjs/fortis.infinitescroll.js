/* FORTIS Infinite Scroll JS ---------------------------------------------------------------------
------------------------------------------------------------------------------------------------*/
/*
	This LIB adds two way infinite scrolling support to card hubs

	TODO:
		- if we are scrolled to the bottom of the page, make sure the page indicator is updated
*/

(function() {
	"use strict";

	Fortis = window.Fortis || {};

	Fortis.infiniteScroller = {

		// PARAMETERS ----------------------------------------------------------------------------
		//these parameters must be passed to the init() method

		params: {
 			//params for link management
 			pageRoot:            null,
 			articles_per_page:   null,

 			//params for accessing the db
 			page:                null,
 			total_content_count: null,
 			total_pages:         null,
 			filter:              null,
		},

		// INTERNAL CONFIG ----------------------------------------------------------------------------
		//these are settings used only by this library

		config: {
			distance:      1200,
			pagesinmemory:    Fortis.config.cardPagesInMemory,
			referenceTop:  null,

			endpoint: function(which, id, filter) {
				//this endpoint returns a subset of data in html form
				var endpoint = '/command/html/{route_command}/{id}/{filter}?stamp=' + $.now();
				var routeCommand = (which == "top") ? "newer_than_id" : "older_than_id";

				return endpoint.replace("{route_command}",routeCommand).replace("{id}",id).replace("{filter}", filter);
			}
		},

		// ELEMENT SELECTORS -------------------------------------------------------------------------
		//these are the jquery selectors we will use to target elements

		el: {
			rtt:       '.return-to-top',
			container: '#article-entry-point',
			topbtn:    '#load-newer-pager',
			botbtn:    '#load-more-pager',
			card:      '.cardset',
		},

		// STATUS MANAGEMENT -------------------------------------------------------------------------
		//these track the current state of the library

		status: {
			busy:                false,    //changes to true when we are fetching
			lastScrollDirection: false,
		},

		// DOM LIBRARY -------------------------------------------------------------------------------

		updateRTT: function(page) {
			var that = this;

			//show the label if there is more than one page
			if (this.params.total_pages > 1) {
				//update the rtt label data
				$(this.el.rtt).attr('data-current-page',page).attr('data-total-pages', this.params.total_pages);

				//update the rtt label
				var label = '{page} of {total_pages}';
				label = label.replace('{page}',page).replace('{total_pages}',this.params.total_pages);
				$(this.el.rtt).find('.label .text').html(label).parent().removeClass('disabled');
			}

			return $(this.el.rtt);
		},

		handleNewStoriesAlert: function(total_content_count) {
			//this will create or hide the new story alerts
			var newContent = (total_content_count > parseInt(this.params.total_content_count)) ? true : false;
			if (newContent) {
				//we have new content, so let's notify the user
				//set up the label
				var count     = total_content_count - parseInt(this.params.total_content_count);
				var storytext = (count < 2) ? 'Story' : 'Stories';
				var label     = '<i class="fi-arrow-up"></i> {count} New {storytext}';
				label         = label.replace('{count}',count).replace('{storytext}',storytext);

				//apply the label
				$(this.el.topbtn).parent().find('a.loader').html(label).addClass('new');
				$(this.el.rtt).find('.label .new-content-label').html(label).removeClass('hidden');
				return true;
			} else {
				//we do not have new content, so let's hide the notifications
				$(this.el.topbtn).parent().find('a.loader').removeClass('new');
				$(this.el.rtt).find('.label .new-content-label').addClass('hidden');
				return false;
			}
		},

		prepareButtons: function() {
			//set the next-page attribute on the loader buttons
			var newerPage = parseInt(this.params.page) - 1;
			var olderPage = parseInt(this.params.page) + 1;

			$(this.el.topbtn).attr('data-next-page', newerPage);
			$(this.el.botbtn).attr('data-next-page', olderPage);
		},

		loadingButtons: function(which) {
			var el = (which == 'top') ? $(this.el.topbtn) : $(this.el.botbtn);

			el.html('<i class="fi-refresh"></i> Getting More...').addClass('disabled').removeClass('card-shadow');
		},

		updateButtons: function(hasAlerts) {
			var that = this;
			if (hasAlerts || parseInt($(that.el.topbtn).attr('data-next-page')) > 0 ) {
				$('.load-newer-container').show();

				if (hasAlerts) {
					$('.load-newer-container a.loader').show();
				} else {
					$('.load-newer-container a.loader').hide();
				}

				if (parseInt($(that.el.topbtn).attr('data-next-page')) > 0) {
					$('#load-newer-pager').html('<i class="fi-arrow-up"></i> Show More Recent').addClass('card-shadow').removeClass('disabled').show();
				} else {
					$('#load-newer-pager').html('<i class="fi-check"></i> All caught up!').addClass('disabled').removeClass('card-shadow').hide();
				}

				if (hasAlerts && parseInt($(that.el.topbtn).attr('data-next-page')) > 0) {
					$('.load-newer-container').addClass('group');
				} else {
					$('.load-newer-container').removeClass('group');
				}
			} else {
				$('.load-newer-container').hide();
			}

			if (parseInt($(that.el.botbtn).attr('data-next-page')) > that.params.total_pages) {
				$(that.el.botbtn).html('<i class="fi-check"></i> All caught up!').addClass('disabled').removeClass('card-shadow');
			} else {
				$(that.el.botbtn).html('<i class="fi-arrow-down"></i> Show Older Content').removeClass('disabled').addClass('card-shadow');
			}
		},

		prepareCards: function(which) {
			var that = this, loadedPage;

			if (!which) {
				/*
					this is the first time preparing cards. lets send that signal so fortis.js can
					handle scrolling to the last viewed content if needed
				*/
				loadedPage = parseInt(that.params.page);
				$.event.trigger({ type: 'firstCardLoad' });
			} else {
				loadedPage = (which == 'top') ? parseInt($(this.el.topbtn).attr('data-next-page')) : parseInt($(this.el.botbtn).attr('data-next-page'));
			}

			//add date and page data to each card
			$(this.el.card + ":not([data-fortis-prepped])").each(function() {
				//append information and run tasks
				Fortis.parser.datetime($(this).find(".timeago"));
				$(this).attr('data-fortis-page', loadedPage);
			});

			//create waypoints if we have history support
			if (History.enabled) {
				//add waypoints for each page
				var firstPage = $(that.el.card).first().attr('data-fortis-page');
				var lastPage  = $(that.el.card).last().attr('data-fortis-page');

				var i;
				for (i=firstPage; i <= lastPage; i++) {				
					//first unprepped card of the page
					$(this.el.card + "[data-fortis-page='" + i + "']:not([data-fortis-prepped='1'])").first().waypoint(function(direction) {
						if (!$('.full-screen-canvas').hasClass("open")) {
							if ($(this.element).attr('data-fortis-page') == '1') {
								History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot);
							} else {
								History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot + "?page=" + $(this.element).attr('data-fortis-page'));
							}
							that.updateRTT($(this.element).attr('data-fortis-page'));
						}
					}, {offset:'25%'});

					//last unprepped card of the page
					$(this.el.card + "[data-fortis-page='" + i + "']:not([data-fortis-prepped='1'])").last().waypoint(function(direction) {
						if (!$('.full-screen-canvas').hasClass("open")) {
							if ($(this.element).attr('data-fortis-page') == '1') {
								History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot);
							} else {
								History.replaceState({data: 'card-state', canvas: 'false', expect:'.load-newer-container'}, document.title, Fortis.params.pageRoot + "?page=" + $(this.element).attr('data-fortis-page'));
							}
							that.updateRTT($(this.element).attr('data-fortis-page'));
						}
					}, {offset:'25%'});
				}
			}

			//mark the cards as prepped
			$(this.el.card + ":not([data-fortis-prepped])").attr('data-fortis-prepped', 1);

			//reflow content by triggering window resize
			//$(window).trigger('resize');
		},

		addNewPage: function(which, data) {
			var that = this;
			Waypoint.destroyAll();
			$(this.el.container).find(this.el.card + "[data-fortis-prepped='1']").attr('data-fortis-prepped','0');

			//Remove Extra Pages
			var firstPage = parseInt( $(this.el.card).first().attr('data-fortis-page') );
			var lastPage  = parseInt( $(this.el.card).last().attr('data-fortis-page')  );
			var pageCount = lastPage - firstPage + 1 + 1;
			var extraPages = pageCount - this.config.pagesinmemory;

			var i;
			if (extraPages > 0) {
				if (which == 'top') {
					//newer, so delete from bot
					var lastPage = parseInt($(this.el.card).last().attr('data-fortis-page'));
					//$(that.el.container).hide();
					for (i = lastPage; extraPages > 0; i--) {
						$(".content-card[data-fortis-page='" + i + "']").detach();
						extraPages = extraPages - 1;
					}
				} else {
					//older, so delete from top
					var firstPage = parseInt($(this.el.card).first().attr('data-fortis-page'));
					//$(that.el.container).hide();
					for (i = firstPage; extraPages > 0; i++) {
						$(".content-card[data-fortis-page='" + i + "']").detach();
						extraPages = extraPages - 1;
					}
				}
			}

			/*
			var that = this;
			var firstPage = parseInt( $(this.el.card).first().attr('data-fortis-page') );
			var lastPage  = parseInt( $(this.el.card).last().attr('data-fortis-page')  );
			var pageCount = lastPage - firstPage + 1 + 1;
			var extraPages = pageCount - this.config.pagesinmemory;

			if (extraPages > 0) {
				var i;				
				if (which == 'top') {
					//newer, so delete from bot
					var lastPage = parseInt($(this.el.card).last().attr('data-fortis-page'));
					//$(that.el.container).hide();
					for (i = lastPage; extraPages > 0; i--) {
						$(parsed).find(".content-card[data-fortis-page='" + i + "']").remove();
						extraPages = extraPages - 1;
					}
				} else {
					//older, so delete from top
					var firstPage = parseInt($(this.el.card).first().attr('data-fortis-page'));
					//$(that.el.container).hide();
					for (i = firstPage; extraPages > 0; i++) {
						$(parsed).find(".content-card[data-fortis-page='" + i + "']").remove();
						extraPages = extraPages - 1;
					}
				}

				$(parsed).find(this.el.card + "[data-fortis-prepped='1']").attr('data-fortis-prepped','0');
			}
			*/

			//Add New Pages
			var newContent = [], i;
			for (i = 0; i < data.results.length; i++) {
				newContent.push(data.results[i]);
			}
			if (which == "top") {
				//adding content to the top
				newContent.reverse();
				$(this.el.container).prepend(newContent.join(' '));
			} else {
				//adding content to the bottom
				$(this.el.container).append(newContent.join(' '));
			}
		},

		addFiller: function(mode, el, count) {
			var fillerCards = [], page, i;
			var theHtml     = '<div class="medium-4 columns panel content-card card-filler cardset tighten" data-fortis-page="{page}" data-fortis-prepped="1"><a href="#refresh"><i class="fi-alert"></i><br />Some Content Was Removed. Click Here To Refresh The List.</a></div>';
			if (mode == 'replace') {
				var page = parseInt($(el).attr('data-fortis-page'));
				theHtml  = theHtml.replace('{page}', page);
				$(el).after(theHtml);
				$(el).detach();
			} else if (mode == 'prepend') {
				var page = parseInt($(el).attr('data-fortis-page'));
				theHtml  = theHtml.replace('{page}', page);
				for (i=0; i < count; i++) {
					fillerCards.push(theHtml);
				}
				$(this.el.container).prepend(fillerCards.join(' '));
			}
		},

		// METHOD LIBRARY ----------------------------------------------------------------------------

		canLoadTop: function(checkScrollDirection) {
			// this checks to see if we are within the correct distance to initiate a top load
			if (parseInt($(this.el.topbtn).attr('data-next-page')) < 1 || this.status.busy) { return false; }

			var fromTop = $(window).scrollTop() - $(this.el.container).offset().top;

			if (fromTop <= this.config.distance && !$('.full-screen-canvas').hasClass('open')) {
				return fromTop;
			} else {
				return false;
			}
		},

		canLoadBot: function(checkScrollDirection) {
			// this checks to see if we are within the correct distance to initiate a bot load
			if (parseInt($(this.el.botbtn).attr('data-next-page')) > parseInt(this.params.total_pages) || this.status.busy) { return false; }

			var winBottom = ($(window).height());
			var elBottom  = $(this.el.container).offset().top + $(this.el.container).outerHeight();
			var fromWinBottom = $(document).height() - ($(window).scrollTop() + $(window).height());
			var space = $(document).height() - elBottom;
			var fromBottom = fromWinBottom - space;

			if (fromBottom <= this.config.distance && !$('.full-screen-canvas').hasClass('open')) {
				return fromBottom;
			} else {
				return false;
			}
		},

		startLoading: function(which) {
			var that = this;

			//mark the library as busy so we don't end up loading more than one at a time
			this.status.busy = true;

			//fetch our data
			this.fetch(which);
		},

		// AJAX HANDLERS -----------------------------------------------------------------------------

		fetch: function(which) {
			var that = this;

			//set up our endpoint
			var id = (which == "bot")
				? $(this.el.card + '[data-fortis-id]').last().attr('data-fortis-id')
				: $(this.el.card + '[data-fortis-id]').first().attr('data-fortis-id');

			var endpoint = this.config.endpoint(which, id, this.params.filter);

			//change the clicked button to a loading state
			that.loadingButtons(which);

			//make the ajax call
			$.ajax({
                type        : "GET",
                url         : endpoint,
                data        : {},
                dataType    : "json",
                success: function(data, status, object) {
                	if (!data.results) {
                		that.handleFailure(which,data,status,object);
                	} else {
                		that.handleSuccess(which,data,status,object);
                	}
                },
                error: function(data, status, object){
                	that.handleFailure(which,data,status,object);
                }
            });
		},

		handleSuccess: function(which, data, status, object) {
			var that = this, isReversed = false, savedDirection, i;

			if (data.total_returned > 0) {
				/*
					We need a point of reference for rescrolling content. If we are
					loading down, we can use the last card as a point of reference.
					If we are loading up, we can use the first card as a point of
					reference.
				*/
				var ref       = (which == 'top') ? $(this.el.card).first() : $(this.el.card).last();
				var refID     = ref.attr('data-fortis-id');
				var fromVPTop = ref.offset().top - $(document).scrollTop();

				//is there new content we should alert for?
				var hasAlerts = this.handleNewStoriesAlert(parseInt(data.total_content_count));

				//destroy older pages first, or waypoints will activate
				//this.destroyExtraPages(which);

				//lets add our new content
				this.addNewPage(which, data);

				//prepare and reveal the cards
				this.prepareCards(which);
				Fortis.fn.showContentCards(isReversed);

				//add filler for deleted content
				if (which == "top" && parseInt(data.total_returned) < this.params.articles_per_page) {
					var deletedCount = this.params.articles_per_page - parseInt(data.total_returned);
					that.addFiller('prepend', $(that.el.card).first(), deletedCount);
				}

				//update our pager data
				$(that.el.topbtn).attr('data-next-page', (parseInt($(that.el.card).first().attr('data-fortis-page')) - 1));
				$(that.el.botbtn).attr('data-next-page', (parseInt($(that.el.card).last().attr('data-fortis-page'))  + 1));

				//show or hide ui elements
				that.updateButtons(hasAlerts);

				//trigger image filling
				$(document).trigger('image-backfill');

				//do we need to rescroll content?
				ref              = $(this.el.card + "[data-fortis-id='" + refID + "']");
				var fromVPTopN   = ref.offset().top - $(document).scrollTop();
				var difference   = fromVPTopN - fromVPTop;
				var newScrollPos = $(document).scrollTop() + difference;
				if (difference !== 0) {
					//we need to rescroll our content
					$(document).scrollTop(newScrollPos);
					//window.scrollTo($(window).scrollLeft(), newScrollPos);
					that.status.lastScrollDirection = (which == "top") ? 'up' : 'down';
				}
			}
			this.handleCleanup();
		},

		handleFailure: function(which, data, status, object) {
			//todo
			this.handleCleanup();
		},

		handleCleanup: function(which) {
			var that = this;

			setTimeout(function() {
				//add disqus comment counts
				if (window.DISQUSWIDGETS) { window.DISQUSWIDGETS.getCount({reset:true}); }
			}, 500);

			//release the library
			that.status.busy = false;
		},

		// EVENT HANDLERS ----------------------------------------------------------------------------

		registerEvents: function() {
			var that = this;

			//register with our scroll monitor
			/*
			Fortis.params.scrollMonitor.push(function(direction) {
				if (that.status.busy || (that.status.lastScrollDirection !== direction)) {
					that.status.lastScrollDirection = direction;
					return;
				} else {
					that.status.lastScrollDirection = direction;
					if (direction == "up") {
						if (that.canLoadTop(true)) { that.startLoading('top'); }
					} else if (direction == "down") {
						if (that.canLoadBot(true)) { that.startLoading('bot'); }
					}
				}
			});
			*/

			//register our button clicks
			$('body').on('click', that.el.topbtn, function() {
				if (that.canLoadTop()) {
					that.startLoading('top');
				}
			});
			$('body').on('click', that.el.botbtn, function() {
				if (that.canLoadBot()) {
					that.startLoading('bot');
				}
			});
		},

		// INITIALIZATION ----------------------------------------------------------------------------

		init: function(params) {
			this.params = params;
			this.prepareButtons();
			this.prepareCards();
			this.registerEvents();

			//preload newer content if we're linked to a specific page
			if (parseInt(this.params.page) > 1) {
				$(this.el.topbtn).click();
			}

			this.updateRTT(this.params.page);
		}
	};
})();