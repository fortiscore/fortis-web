<?php
namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /* DEFAULT FIELDS ======================================================
    ======================================================================*/
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /* CUSTOM FIELDS =======================================================
    ======================================================================*/
    /**
     * @ORM\Column(type="boolean")
     */
    protected $verified = false;

    /**
     * @ORM\Column(type="string", length=30, unique=true, nullable=true)
     */
    protected $displayName = null;

    /* CONSTRUCTOR =========================================================
    ======================================================================*/
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /* GETTER SETTER =======================================================
    ======================================================================*/
    //verified
    public function getDisplayName()
    {
        return $this->displayName;
    }

    public function setDisplayName($name)
    {
        $this->displayName = $name;

        return $this;
    }
    //verified
    public function isVerified()
    {
        return $this->verified;
    }

    public function setVerified($boolean)
    {
        $this->verified = (Boolean) $boolean;

        return $this;
    }

    public function getGravatar($size = 30)
    {
        $email = $this->email;

        return "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $this->email ) ) ) . "?&s=" . $size;

    }
}