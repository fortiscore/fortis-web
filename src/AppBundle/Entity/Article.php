<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\Marquee;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ArticleRepository")
 * @ORM\Table(name="fc_article")
 * @UniqueEntity("slug_url")
 */
class Article
{
    // entity config ===============================================
    //==============================================================

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $slug_url;

    /**
     * @ORM\Column(type="string", length=365, nullable=true)
     */
    protected $meta1;

    /**
     * @ORM\Column(type="string", length=365, nullable=true)
     */
    protected $meta2;

    /**
     * @ORM\Column(type="string", length=365, nullable=true)
     */
    protected $meta3;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $body;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $markdown = null;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $modified = null;

    /**
     * @ORM\Column(type="string")
     * @Assert\Url()
     */
    protected $thumbnail;

    /**
     * @ORM\Column(type="integer")
     */
    protected $author_id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
    */
    protected $author;

    // get and set =================================================
    //==============================================================

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $title
     * @return Article
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set slug_url
     *
     * @param string $slugUrl
     * @return Article
     */
    public function setSlugUrl($slugUrl)
    {
        $this->slug_url = $slugUrl;

        return $this;
    }

    /**
     * Get slug_url
     *
     * @return string 
     */
    public function getSlugUrl()
    {
        return $this->slug_url;
    }

    /**
     * Set meta1
     *
     * @param string $meta1
     * @return Article
     */
    public function setMeta1($meta1)
    {
        $this->meta1 = $meta1;

        return $this;
    }

    /**
     * Get meta1
     *
     * @return string 
     */
    public function getMeta1()
    {
        return $this->meta1;
    }

    /**
     * Set meta2
     *
     * @param string $meta2
     * @return Article
     */
    public function setMeta2($meta2)
    {
        $this->meta2 = $meta2;

        return $this;
    }

    /**
     * Get meta2
     *
     * @return string 
     */
    public function getMeta2()
    {
        return $this->meta2;
    }

    /**
     * Set meta3
     *
     * @param string $meta3
     * @return Article
     */
    public function setMeta3($meta3)
    {
        $this->meta3 = $meta3;

        return $this;
    }

    /**
     * Get meta3
     *
     * @return string 
     */
    public function getMeta3()
    {
        return $this->meta3;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription($length = 320, $html = true)
    {
    	$html = false; //todo - to add line breaks this needs to be set to true and the concatonation needs to NOT happen in the middle of an html tag
    	$description = $this->markdown;

    	if ($html) {
	    	$description = strip_tags($description,"<p>");
    		$description = str_replace("<p>","<br />", $description);
    		$description = str_replace("</p>","", $description);
    	} else {
    		$description = strip_tags($description);
    	}

    	$strlength   = strlen($description);
    	$description = substr($description, 0, $length);

    	if ($strlength > $length) {
    		$description = $description." ...";
    	}

        return $description;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Article
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set markdown
     *
     * @param string $markdown
     * @return Article
     */
    public function setMarkdown($markdown)
    {
        $this->markdown = $markdown;

        return $this;
    }

    /**
     * Get markdown
     *
     * @return string 
     */
    public function getMarkdown()
    {
        return $this->markdown;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Article
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Article
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return Article
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set author_name
     *
     * @param string
     * @return Article
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author_name
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set author_id
     *
     * @param string $authorId
     * @return Article
     */
    public function setAuthorId($authorId)
    {
        $this->author_id = $authorId;

        return $this;
    }

    /**
     * Get author_id
     *
     * @return string 
     */
    public function getAuthorId()
    {
        return $this->author_id;
    }


    public function getWeblink()
    {
    	$datetime   = $this->created;
        $id         = $this->id;
        $type       = $this->type;
        if (empty($type)) {
        	$type = 'article';
        }

    	$dateYear   = date_format($datetime, 'Y');
    	$dateMonth	= date_format($datetime, 'm');
    	$dateDay	= date_format($datetime, 'd');

    	return '/' . $type . '/view/' . $dateYear . '/' . $dateMonth . '/' . $dateDay . '/' . $id . '/' . $this->slug_url;
    }
}
