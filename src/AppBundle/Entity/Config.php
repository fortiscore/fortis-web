<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use AppBundle\Entity\Marquee;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ConfigRepository")
 * @ORM\Table(name="fc_config")
 */
class Config
{
    // entity config ===============================================
    //==============================================================

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $varname;

    /**
     * @ORM\Column(type="string", length=365, nullable=true)
     */
    protected $setting1;

    /**
     * @ORM\Column(type="string", length=365, nullable=true)
     */
    protected $setting2;

    /**
     * @ORM\Column(type="string", length=365, nullable=true)
     */
    protected $setting3;

    /**
     * @ORM\Column(type="string", length=365, nullable=true)
     */
    protected $setting4;

    // get and set =================================================
    //==============================================================

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set varname
     *
     * @param string $varname
     * @return Config
     */
    public function setvarname($varname)
    {
        $this->varname = $varname;

        return $this;
    }

    /**
     * Get varname
     *
     * @return string 
     */
    public function getVarname()
    {
        return $this->varname;
    }

    /**
     * Set setting1
     *
     * @param string $setting1
     * @return Config
     */
    public function setSetting1($setting1)
    {
        $this->setting1 = $setting1;

        return $this;
    }

    /**
     * Get setting1
     *
     * @return string 
     */
    public function getSetting1()
    {
        return $this->setting1;
    }

    /**
     * Set setting2
     *
     * @param string $setting2
     * @return Config
     */
    public function setSetting2($setting2)
    {
        $this->setting2 = $setting2;

        return $this;
    }

    /**
     * Get setting2
     *
     * @return string 
     */
    public function getSetting2()
    {
        return $this->setting2;
    }

    /**
     * Set setting3
     *
     * @param string $setting3
     * @return Config
     */
    public function setSetting3($setting3)
    {
        $this->setting3 = $setting3;

        return $this;
    }

    /**
     * Get setting3
     *
     * @return string 
     */
    public function getSetting3()
    {
        return $this->setting3;
    }

    /**
     * Set setting4
     *
     * @param string $setting4
     * @return Config
     */
    public function setSetting4($setting4)
    {
        $this->setting4 = $setting4;

        return $this;
    }

    /**
     * Get setting4
     *
     * @return string 
     */
    public function getSetting4()
    {
        return $this->setting4;
    }
}
