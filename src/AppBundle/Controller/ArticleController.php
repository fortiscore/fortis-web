<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArticleController extends Controller
{
	# view a single article =======================================
	# =============================================================
	public function viewAction($type, $year, $month, $day, $id, $slug_url) {
        //test
		//setup requirements
		$dateString = $year.'-'.$month.'-'.$day;
		$repository = $this->getDoctrine()->getRepository('AppBundle:Article');

		//grab the article and assign the result
		$result = $repository->getOneById($id);
		$article = (!empty($result)) ? $result[0] : false;

        if (!$article) {
            throw $this->createNotFoundException('The product does not exist');
        }

        //add the disqus url for the page
        $disqus_enabled = $this->container->getParameter('disqus_enabled');
        if ($disqus_enabled == 1) {
            $disqus_url = ($this->container->getParameter('webroot') . $this->generateUrl('content_view', array(
                'type'     => $type,
                'year'     => $year,
                'month'    => $month,
                'day'      => $day,
                'id'       => $id,
                'slug_url' => $slug_url,
            )));
        } else {
            $disqus_url = false;
        }

		// Parse markdown using parsedown standart parser.
		//echo $container->get('parsedown.standart')->text($var);

		// Parse markdown using parsedown-extra parser.
		//echo $container->get('parsedown.extra')->text($var);

        $markedDown = $this->container->get('parsedown.standart')->text($article->getBody());
        $article->setBody($markedDown);

		//render the page
        return $this->render('card_page/'.$type.'.html.twig', array(
            'article'	 => $article,
            'disqus_url' => $disqus_url
        ));
	}

    # handle rss feed =============================================
    # =============================================================
    public function feedAction($filter) {
        //get content based on the filter
        $repository = $this->getDoctrine()->getRepository('AppBundle:Article');
        $result = $repository->getFeed($filter);
        $type = $filter;

        if ($filter == "" || empty($filter) || $filter == "*") {
            $filter = "*";
            $type = "All Content";
        } else {
            if ($type == 'news') {
                $type = ucfirst($type);
            } else {
                $type = ucfirst($type) . "s";
            }
        }

        $recentContent = (!empty($result) ? $result[0] : false);
        if ($recentContent) {
            $lastBuildDate = $recentContent->getCreated()->format("D, d M Y H:i:s O");
        } else {
            $lastBuildDate = null;
        }

        $feed = $this->renderView('feed/all.html.twig', array(
            'results' =>       $result,
            'filter'  =>       $filter,
            'type'    =>       $type,
            'lastBuildDate' => $lastBuildDate
        ));

        $response = new Response();
        $response->setContent($feed);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }

    # fetch articles in html chunks ===============================
    # =============================================================
    public function htmlNewerThanIdAction ($id, $filter) {
        //setup cache
        $response = new JsonResponse();
        $response->setPublic();
        $response->setSharedMaxAge(300); //in seconds
        $response->headers->addCacheControlDirective('must-revalidate', true);

        //setup requirements
        $ArticlesPerPage = $this->container->getParameter('articles_per_page');

        $repository  = $this->getDoctrine()->getRepository('AppBundle:Article');
        $total_pages = ceil($repository->getCount($filter) / $ArticlesPerPage);
        $result      = $repository->getNewerThanId($id, $ArticlesPerPage, $filter);

        //set up our return data
        $articles['total_returned']      = sizeof($result);
        $articles['total_pages']         = $total_pages;
        $articles['total_content_count'] = (int) $repository->getCount($filter);
        $articles['results']             = array();
        foreach ($result as $article) {
            $articles['results'][] = $this->renderView('card/_types.card.twig', array(
                'article'    => $article
            ));
        }

        //are we at the end of the file?
        if ($articles['total_returned'] < $ArticlesPerPage) {
            $articles['EOF'] = true;
        } else {
            $articles['EOF'] = false;
        }

        $response->setData($articles);
        return $response;
    }

    public function htmlOlderThanIdAction ($id, $filter) {
        //setup cache
        $response = new JsonResponse();
        $response->setPublic();
        $response->setSharedMaxAge(300); //in seconds
        $response->headers->addCacheControlDirective('must-revalidate', true);

        //setup requirements
        $ArticlesPerPage = $this->container->getParameter('articles_per_page');

        $repository = $this->getDoctrine()->getRepository('AppBundle:Article');
        $total_pages = ceil($repository->getCount($filter) / $ArticlesPerPage);
        $result = $repository->getOlderThanId($id, $ArticlesPerPage, $filter);

        //set up our return data
        $articles['total_returned']      = sizeof($result);
        $articles['total_pages']         = $total_pages;
        $articles['total_content_count'] = (int) $repository->getCount($filter);
        $articles['results']             = array();
        foreach ($result as $article) {
            $articles['results'][] = $this->renderView('card/_types.card.twig', array(
                'article'    => $article
            ));
        }

        //are we at the end of the file?
        if ($articles['total_returned'] < $ArticlesPerPage) {
            $articles['EOF'] = true;
        } else {
            $articles['EOF'] = false;
        }

        $response->setData($articles);
        return $response;
    }

    /*
    public function chunkAction($page, $last_id) {
        //setup requirements

        $ArticlesPerPage = $this->container->getParameter('articles_per_page');
        $response = new JsonResponse();

        $repository = $this->getDoctrine()->getRepository('AppBundle:Article');

        //grab the list of articles
        $count = $repository->getCount();
        //$offset = ($page - 1) * $ArticlesPerPage;
        $result = $repository->getList(0, $ArticlesPerPage, $last_id);

        //set up our return data
        $articles['total_articles'] = $count;
        $articles['total_returned'] = sizeof($result);

        foreach($result as $id => $object) {
            $result[$id]->at_page = $page;
        }

        if ($articles['total_returned'] < $ArticlesPerPage) {
            $articles['EOF'] = true;
        } else {
            $articles['EOF'] = false;
        }

        if ($articles['total_returned'] > 0) {
            $articles['last_id'] = end($result)->getId();
        }

        $articles['results'] = array();

        foreach ($result as $article) {
            $articles['results'][] = $this->renderView('card/_types.card.twig', array(
                'article'    => $article
            ));
        }
        //$articles['results'] = $result;

        //return the list
        $response->setData($articles);
        return $response;
    }
    */

	# view list of articles =======================================
	# =============================================================
	public function listAction($page) {
		//setup requirements
        $ArticlesPerPage = $this->container->getParameter('articles_per_page');
        $filter = "*";
    	$response = new JsonResponse();
		$repository = $this->getDoctrine()->getRepository('AppBundle:Article');

		//grab the list of articles
		$count = $repository->getCount();
		$offset = ($page - 1) * $ArticlesPerPage;
		$result = $repository->getList($offset, $ArticlesPerPage, $filter);
		$articles['total_articles'] = $count;
		$articles['total_returned'] = sizeof($result);
		$articles['results'] = $result;

		//return the list
		$response->setData($articles);
		return $response;
	}

    # new article form ============================================
    #==============================================================
    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function newAction()
    {
        return $this->render('article/new.html.twig', array(
        	
        ));
    }

	# edit article form ===========================================
	#==============================================================
    /**
    * @Security("has_role('ROLE_USER')")
    */
	public function editAction($id)
	{
        //setup requirements
        $repository = $this->getDoctrine()->getRepository('AppBundle:Article');

        //grab the article and assign the result
        $result = $repository->getOneById($id);
        $article = (!empty($result) ? $result[0] : false);

        //verify ownership
        if ($article->getAuthor() == $this->getUser() || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->render('article/new.html.twig', array(
                'article'=> $article
            ));
        } else {
            throw new HttpException(401, "Unauthorized Access");
        }
	}

	# save article to db ==========================================
	#==============================================================
    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function createAction(Request $request)
    {
    	// prepare the response
    	$response = new JsonResponse();
        $purifier = $this->container->get('exercise_html_purifier.default');

    	// obtain the submitted article
        $markedDown = htmlspecialchars($request->request->get('body'));
        $markedDown = $purifier->purify($markedDown);
        $markedDown = preg_replace('/\n&gt;/', '>', $markedDown); //save right bracket on new line for blockquote
        $markedDown = $this->container->get('parsedown.standart')->text($markedDown);

    	$articleData = array(
    		'type'		  => $purifier->purify($request->request->get('type')),
    		'description' => $purifier->purify($request->request->get('description')),
    		'title'		  => $purifier->purify($request->request->get('title')),
    		'thumbnail'	  => $purifier->purify($request->request->get('thumbnail')),
    		'body'		  => $request->request->get('body'),
    		'markdown'    => $markedDown
    	);

    	// verify the required fields are set
    	if (empty($articleData['title']) || empty($articleData['body'])) {
    		$response->setData(array(
    			'error' => 'Article, Title, and Description cannot be empty.'
    		));

    		return $response;
    	}

    	if (empty($articleData['type'])) {
    		$articleData['type'] = 'article';
    	}

        // lock podcast submissions down to administrators only
        if ($articleData['type'] == 'podcast') {
            $isAdmin = in_array('ROLE_ADMIN', $this->getUser()->getRoles());
            if ($isAdmin == false) {
            $response->setData(array(
                'error' => 'You do not have permission to do that.'
            ));

            return $response;
            }
        }

    	// form the article object
    	$user           = $this->getUser();
    	$datetime		= date_create();
    	$dateYear		= date_format($datetime, 'Y');
    	$dateMonth		= date_format($datetime, 'm');
    	$dateDay		= date_format($datetime, 'd');
    	$articleSlug	= $this->_slugify($articleData['title']);

    	$article = new Article();
    	$article->setType(			$articleData['type']					);
    	$article->setTitle(			$articleData['title']					);
    	$article->setBody(			$articleData['body']					);
        $article->setMarkdown(      $articleData['markdown']                );
    	$article->setSlugUrl(		$articleSlug							);
    	$article->setCreated(		$datetime								);
    	$article->setThumbnail(		$articleData['thumbnail']				);
    	$article->setAuthor(        $user                                   );
        $article->setType(          $request->request->get('type')          );
        $article->setMeta1(         $request->request->get('meta1')         );
        $article->setMeta2(         $request->request->get('meta2')         );
        $article->setMeta3(         $request->request->get('meta3')         );

    	//attempt to submit the article object
	    $em = $this->getDoctrine()->getManager();
	    try {
	    	$em->persist($article);
	    	$em->flush();
		} catch (\Doctrine\DBAL\DBALException $e) {
    		$response->setData(array(
    			'error' => $e->getMessage()
    		));

    		return $response;
		}


        $type = $article->getType();
        if (empty($type)) {
            $type = 'article';
        }

    	// send the success data
    	$response->setData(array(
    		'url' 		=> '/'.$type.'/view/' . $dateYear . '/' . $dateMonth . '/' . $dateDay . '/' . $article->getId() . '/' . $articleSlug
    	));

    	return $response;
    }

    # save article to db ==========================================
    #==============================================================
    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function updateAction($id)
    {
        $request = $this->getRequest();

        // prepare the response
        $response = new JsonResponse();
        $purifier = $this->container->get('exercise_html_purifier.default');

    	// obtain the submitted article
        $markedDown = htmlspecialchars($request->request->get('body'));
        $markedDown = $purifier->purify($markedDown);
        $markedDown = preg_replace('/\n&gt;/', '>', $markedDown); //save right bracket on new line for blockquote
        $markedDown = $this->container->get('parsedown.standart')->text($markedDown);

        $articleData = array(
            'type'        => $purifier->purify($request->request->get('type')),
            'title'       => $purifier->purify($request->request->get('title')),
            'description' => $purifier->purify($request->request->get('description')),
            'thumbnail'   => $purifier->purify($request->request->get('thumbnail')),
            'body'        => $request->request->get('body'),
            'markdown'    => $markedDown
        );

        // verify the required fields are set
        if (empty($articleData['body'])) {
            $response->setData(array(
                'error' => 'Article, Title, and Description cannot be empty.'
            ));

            return $response;
        }

        // lock podcast submissions down to administrators only
        if ($articleData['type'] == 'podcast') {
            $isAdmin = in_array('ROLE_ADMIN', $this->getUser()->getRoles());
            if ($isAdmin == false) {
            $response->setData(array(
                'error' => 'You do not have permission to do that.'
            ));

            return $response;
            }
        }

        //setup requirements
        $repository = $this->getDoctrine()->getRepository('AppBundle:Article');

        //grab the article and assign the result
        $result = $repository->getOneById($id);
        $article = (!empty($result) ? $result[0] : false);

        $article->setTitle(         $articleData['title']                   );
        $article->setThumbnail(     $articleData['thumbnail']               );
        $article->setBody(          $articleData['body']                    );
        $article->setMarkdown(      $articleData['markdown']                );
    	$article->setType(          $request->request->get('type')          );
    	$article->setMeta1(         $request->request->get('meta1')         );
        $article->setMeta2(         $request->request->get('meta2')         );
        $article->setMeta3(         $request->request->get('meta3')         );

        //attempt to submit the article object
        $em = $this->getDoctrine()->getManager();
        try {
            $em->persist($article);
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            $response->setData(array(
                'error' => $e->getMessage()
            ));

            return $response;
        }

        $datetime       = $article->getCreated();
        $dateYear       = date_format($datetime, 'Y');
        $dateMonth      = date_format($datetime, 'm');
        $dateDay        = date_format($datetime, 'd');
        $articleSlug    = $this->_slugify($article->getTitle());

        $type = $article->getType();
        if (empty($type)) {
            $type = 'article';
        }

        $response->setData(array(
            'success' => true,
            'url'     => '/' . $type . '/view/' . $dateYear . '/' . $dateMonth . '/' . $dateDay . '/' . $article->getId() . '/' . $articleSlug
        ));

        return $response;       
    }

    # save article to db ==========================================
    #==============================================================
    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function deleteAction($id)
    {
        $request = $this->getRequest();

        // prepare the response
        $response = new JsonResponse();

        //setup requirements
        $repository = $this->getDoctrine()->getRepository('AppBundle:Article');

        //grab the article and assign the result
        $result = $repository->getOneById($id);
        $article = (!empty($result) ? $result[0] : false);

        //verify ownership
        if ($article->getAuthor() == $this->getUser() || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            //nothing, continue as expected
        } else {
            throw new HttpException(401, "Unauthorized Access");
        }

        //attempt to submit the article object
        $em = $this->getDoctrine()->getManager();
        try {
            $em->remove($article);
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            $response->setData(array(
                'error' => $e->getMessage()
            ));

            return $response;
        }

        $response->setData(array(
            'success' => true
        ));

        return $response;
    }

    public function _slugify($text)
    {
    	// replace non letter or digits by -
	    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
	    // trim
	    $text = trim($text, '-');
	    // transliterate
	    if (function_exists('iconv'))
	    {
	        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	    }
	    // lowercase
	    $text = strtolower($text);
	    // remove unwanted characters
	    $text = preg_replace('~[^-\w]+~', '', $text);
	    if (empty($text))
	    {
	        return 'n-a';
	    }
	    return $text;
    }
}