<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class HubController extends Controller
{
    //EMAIL-----------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function sendEmailAction(Request $request)
    {
        $response = new JsonResponse();

        $name = $request->request->get('name');
        $email = $request->request->get('email');
        $message = $request->request->get('message');

        $responseObj = array();

        if (empty($name) || empty($email) || empty($message)) {
          $response->setStatusCode(400);
          $responseObj['response'] = 'Please fill out the form';
        } else {
          if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response->setStatusCode(400);
            $responseObj['response'] = 'You have entered an invalid email.';
          } else {
            $pMessage = \Swift_Message::newInstance(null)
                ->setSubject('WebForm ['.$name.']')
                ->setFrom($email)
                ->setTo('mailbox@fortiscoregaming.com')
                ->setContentType("text/html")
                ->setBody($message,'text/html');
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )
                */
            ;
            try{
                $mailer = $this->container->get('mailer');
                $mresponse = $this->container->get('mailer')->send($pMessage);
            }catch(\Swift_TransportException $e){
                $mresponse = $e->getMessage();
                $responseObj['error'] = $mresponse;
            }

            $response->setStatusCode(200);
            $responseObj['response'] = 'Your message has been sent!';
            $responseObj['mresponse'] = $mresponse;
          }
        }

        $response->setData($responseObj);
        return $response;
    }

    public function contactAction(Request $request)
    {
        //render the page
        return $this->render('contact.html.twig', array(
            'base_dir'    => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'panelView'   => 'small',
            'panelSplash' => $this->container->getParameter('splash_community'),
        ));
    }
    //DONATE----------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function donateAction(Request $request)
    {
        //render the page
        return $this->render('donate.html.twig', array(
            'base_dir'    => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'panelView'   => 'small',
            'panelSplash' => $this->container->getParameter('splash_community'),
        ));
    }

    //HOME -----------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function indexAction(Request $request)
    {
        return $this->_prepareCardView($request, array(
          'filter'        => '*',
          'web_dir'       => '/',
          'template'      => 'hub/index.html.twig',
          'panelView'     => 'large',
          'panelSplash'   => $this->container->getParameter('splash_home'),
          'spotlightData' => $this->_getSpotlightData()
        ));
    }

    //NEWS -----------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function newsAction(Request $request)
    {
        return $this->_prepareCardView($request, array(
            'filter'        => 'news',
            'web_dir'       => '/news',
            'template'      => 'hub/news.html.twig',
            'panelView'     => 'small',
            'panelSplash'   => $this->container->getParameter('splash_news'),
            'spotlightData' => null
        ));
    }

    //ARTICLES -------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function articlesAction(Request $request)
    {
        return $this->_prepareCardView($request, array(
        	'filter'        => 'article',
        	'web_dir'       => '/articles',
        	'template'      => 'hub/articles.html.twig',
        	'panelView'     => 'small',
        	'panelSplash'   => $this->container->getParameter('splash_articles'),
        	'spotlightData' => null
        ));
    }

    //VIDEOS ---------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function videosAction(Request $request)
    {
        return $this->_prepareCardView($request, array(
            'filter'        => 'video',
            'web_dir'       => '/videos',
            'template'      => 'hub/videos.html.twig',
            'panelView'     => 'small',
            'panelSplash'   => $this->container->getParameter('splash_videos'),
            'spotlightData' => null
        ));
    }

    //REVIEWS --------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function reviewsAction(Request $request)
    {
        return $this->_prepareCardView($request, array(
            'filter'        => 'review',
            'web_dir'       => '/reviews',
            'template'      => 'hub/reviews.html.twig',
            'panelView'     => 'small',
            'panelSplash'   => $this->container->getParameter('splash_reviews'),
            'spotlightData' => null
        ));
    }

    //PODCAST --------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function podcastAction(Request $request)
    {
        return $this->_prepareCardView($request, array(
        	'filter'        => 'podcast',
        	'web_dir'       => '/podcast',
        	'template'      => 'hub/podcast.html.twig',
        	'panelView'     => 'small',
        	'panelSplash'   => $this->container->getParameter('splash_podcast'),
        	'spotlightData' => null
        ));
    }

    public function popoutplayerAction(Request $request, $id)
    {
        return $this->render('hub/popoutplayer.html.twig', array(
            'soundcloud_id' => $id
        ));
    }

    //FOLLOW US ------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function communityAction(Request $request)
    {
        //render the page
        return $this->render('hub/community.html.twig', array(
            'base_dir'    => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'panelView'   => 'small',
            'panelSplash' => $this->container->getParameter('splash_community'),
        ));
    }

    //HELPERS --------------------------------------------------------------------
    //----------------------------------------------------------------------------
    public function _prepareCardView(Request $request, $params) {
        //set up requirements
        $ArticlesPerPage = $this->container->getParameter('articles_per_page');

        //get the repository
        $repository = $this->getDoctrine()->getRepository('AppBundle:Article');

        //determine paging and offsets
        $total_pages = ceil($repository->getCount($params['filter']) / $ArticlesPerPage);
        $page = (int)$request->query->get('page') ? (int)$request->query->get('page') : 1;
        $page = ($page > $total_pages) ? $total_pages : $page;
        $page = ($page < 1           ) ? 1            : $page;
        $offset = ($page - 1) * $ArticlesPerPage;

        //fetch the results
        $result = $repository->getList($offset, $ArticlesPerPage, $params['filter']);

        //format the response
        $articles = array();
        $articles['total_returned']      = sizeof($result);
        $articles['total_content_count'] = (int) $repository->getCount($params['filter']);
        $articles['results']             = $result;

        //sort the results in DESC (higher IDs are newer content; newest first)
        if ($articles['total_returned'] > 0) {
            arsort($articles['results']);
        }

        //determine the view type
        $view = $request->query->get('view') ? $request->query->get('view') : 'scroll';

        //render the page
        return $this->render($params['template'], array(
            'base_dir'      => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'articles'      => $articles,
            'articles_per_page' => $ArticlesPerPage,
            'page'          => $page,
            'total_pages'   => $total_pages,
            'total_content_count' => $articles['total_content_count'],
            'web_dir'       => $params['web_dir'],
            'filter'        => $params['filter'],
            'view'          => $view,
            'panelView'     => $params['panelView'],
            'panelSplash'   => $params['panelSplash'],
            'spotlightData' => $params['spotlightData']
        ));
    }

    public function _getSpotlightData() {
        //get the spotlight article if it exists
        $spotlightData = Array();
        $content = Array();
        $useOverrides = false;
        $configRepo = $this->getDoctrine()->getRepository('AppBundle:Config');
        $result = $configRepo->getSingleConfig('spotlight');
        if (!empty($result)) {
            $config = $result[0];
            $spotlightData['config'] = $config;
            $contentID = $config->getSetting1();
            $overrideLink  = $config->getSetting2();
            $overrideTitle = $config->getSetting3();
            $overrideSplash = $config->getSetting4();

            //assign splash image
            $splash = $config->getSetting4();
            if (!empty($splash) && $splash !== '') {
                $spotlightData['splash'] = $splash;
            }

            //assign any linked content information we need
            if (!empty($contentID) && $contentID !== '') {
                //we are linking to content on our site. let's fetch it
                $contentRepo = $this->getDoctrine()->getRepository('AppBundle:Article');
                $result = $contentRepo->getOneById($contentID);
                $content = (!empty($result)) ? $result[0] : false;

                if (!empty($content)) {
                    //content was found
                    $spotlightData['cardAction'] = true;
                    $spotlightData['title'] = $content->getTitle();
                    $spotlightData['url'] = $content->getWebLink();

                    $thumbnail = $content->getThumbnail();
                    if ($thumbnail !== '') {
                        $spotlightData['splash'] = $thumbnail;
                    } else {
                        $spotlightData['splash'] = '/assets/img/default-article-thumbnail.png';
                    }
                }
            } else {
                $spotlightData['title'] = $overrideTitle;
            }
            //manage spotlight overrides
            if (!empty($overrideLink) && $overrideLink !== '') {
                $spotlightData['cardAction'] = false;
                $spotlightData['url'] = $overrideLink;
            }
            if (!empty($overrideTitle) && $overrideTitle !== '') {
                $spotlightData['overrideTitle'] = $overrideTitle;
            }
            if (!empty($overrideSplash) && $overrideSplash !== '') {
                $spotlightData['splash'] = $overrideSplash;
            }
        }

        return $spotlightData;
    }
}
