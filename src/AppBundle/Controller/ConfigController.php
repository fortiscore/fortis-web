<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Config;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ConfigController extends Controller
{
    /**
    * @Security("has_role('ROLE_ADMIN')")
    */
    public function setAction(Request $request, $varname)
    {
        $response = new JsonResponse();
        $purifier = $this->container->get('exercise_html_purifier.default');

        $data = array(
            'setting1' => $purifier->purify($request->request->get('setting1')),
            'setting2' => $purifier->purify($request->request->get('setting2')),
            'setting3' => $purifier->purify($request->request->get('setting3')),
            'setting4' => $purifier->purify($request->request->get('setting4'))
        );

        $configRepo = $this->getDoctrine()->getRepository('AppBundle:Config');
        $result = $configRepo->getSingleConfig('spotlight');
        if (!empty($result)) {
            //we have the config and we should update it
            $config = $result[0];
            $config->setVarname(           $varname                             );
            $config->setSetting1(          $data['setting1']                    );
            $config->setSetting2(          $data['setting2']                    );
            $config->setSetting3(          $data['setting3']                    );
            $config->setSetting4(          $data['setting4']                    );
        } else {
            //we need to create the config
            $config = new Config();
            $config->setVarname(           $varname                             );
            $config->setSetting1(          $data['setting1']                    );
            $config->setSetting2(          $data['setting2']                    );
            $config->setSetting3(          $data['setting3']                    );
            $config->setSetting4(          $data['setting4']                    );
        }

        //attempt to submit the config object
        $em = $this->getDoctrine()->getManager();
        try {
            $em->persist($config);
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            $response->setData(array(
                'error' => $e->getMessage()
            ));

            return $response;
        }

        // send the success data
        $response->setData(array(
            'success' => true
        ));

        return $response;
    }
}