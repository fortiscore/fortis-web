<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class JarvisController extends Controller
{
	# configuration ===============================================
	# =============================================================
	private $ArticlesPerPage = 10;

	# jarvis index page ===========================================
	# =============================================================
    /**
    * @Security("has_role('ROLE_ADMIN')")
    */
	public function indexAction() {
        //setup requirements
        $repository = $this->getDoctrine()->getRepository('AppBundle:Marquee');

        //grab the marquee slides
        $result = $repository->getSlides();
        $slides = (!empty($result) ? $result : false);

        //grab the list of articles
        $count = 12;
        $offset = 0;
        $repository = $this->getDoctrine()->getRepository('AppBundle:Article');
        $result = $repository->getList($offset, $count);
        $articles['total_articles'] = $count;
        $articles['total_returned'] = sizeof($result);
        $articles['results'] = $result;

        //render the page
        return $this->render('jarvis/jarvis.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'slides'   => $slides,
            'articles' => $articles
        ));
	}

	# jarvis marquee page =========================================
	# =============================================================
    /**
    * @Security("has_role('ROLE_ADMIN')")
    */
	public function marqueeAction() {
        //setup requirements
        $repository = $this->getDoctrine()->getRepository('AppBundle:Marquee');

        //grab the marquee slides
        $result = $repository->getSlides();
        $slides = (!empty($result) ? $result : false);

        //render the page
        return $this->render('jarvis/marquee.html.twig', array(
            //'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'slides'   => $slides
            //'articles' => $articles
        ));
	}

	# jarvis articles page ========================================
	# =============================================================
    /**
    * @Security("has_role('ROLE_ADMIN')")
    */
	public function articlesAction() {

        //render the page
        return $this->render('jarvis/articles.html.twig', array(
            //'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            //'slides'   => $slides,
            //'articles' => $articles
        ));

	}

    public function _slugify($text)
    {
    	// replace non letter or digits by -
	    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
	    // trim
	    $text = trim($text, '-');
	    // transliterate
	    if (function_exists('iconv'))
	    {
	        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
	    }
	    // lowercase
	    $text = strtolower($text);
	    // remove unwanted characters
	    $text = preg_replace('~[^-\w]+~', '', $text);
	    if (empty($text))
	    {
	        return 'n-a';
	    }
	    return $text;
    }
}