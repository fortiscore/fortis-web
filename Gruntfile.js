//GRUNTFILE FOR FORTIS-WEB
(function() {
	'use strict';

  	// # INITIALIZE VARIABLES ----------------------------------
  	//----------------------------------------------------------
	var inflection = require('inflection');
	var fs         = require('fs');
	var config     = {};
	var timestamp  = new Date().getTime();

	var dir = {
		web: "public_html"
	};

	var adminUser = {
		name:     "sudorootuser",
		email:    "fortiscoregaming@gmail.com",
		password: "kru0pl0x2015",
	}

  	// # CONFIGURE GRUNT TASKS ---------------------------------
  	//----------------------------------------------------------
	//grunt-contrib-concat
	config.concat = {
		js: {
			src: [
				'_assets/js/burry.js',
				//'_assets/js/jquery.timeago.js',
				'_assets/js/moment.js',
				'_assets/js/jquery.history.js',
				'_assets/js/jquery.waypoints.min.js',
				'_assets/js/jquery.backstretch.js',
				'_assets/js/textarea-expander.js',
				'_assets/js/foundation.min.js',
				
				'_assets/js/fortisjs/fortis.config.js',
				'_assets/js/fortisjs/fortis.js',
				'_assets/js/fortisjs/fortis.infinitescroll.js',
				'_assets/js/fortisjs/fortis.parser.js',

				'_assets/js/jquery.mobile.custom.min.js'
			],
			dest: '_build/js/app.js'
		},
		css: {
			src: [
				'_assets/css/foundation.css',
				'_assets/css/foundation-icons.css',
				'_build/css/compiled.css',
			],
			dest: '_build/css/app.css'
		},
		sass: {
			src: [
				'_assets/scss/!definitions.scss',
				'_assets/scss/!global.scss',
				'_assets/scss/material_design.scss',
				'_assets/scss/top_panel.scss',
				'_assets/scss/pages.scss',
				'_assets/scss/widgets.scss',
				'_assets/scss/content_cards.scss',
				'_assets/scss/social_media_cards.scss',
				'_assets/scss/rtt.scss',
				'_assets/scss/footer.scss'
			],
			dest: '_build/scss/app.scss'
		}
	};

	//grunt-sass
	config.sass = {
		dist: {
			options: {
				style: 'expanded'
			},
			files: [{
				'_build/css/compiled.css': '_build/scss/app.scss'
			}]
		}
	};

	//grunt-contrib-uglify
	config.uglify = {
		js: {
			src: '_build/js/app.js',
			dest: '_build/js/app.min.js'
		},
	};

	//grunt-contrib-cssmin
	config.cssmin = {
		target: {
			files: {
			  '_build/css/app.min.css': ['_build/css/app.css']
			}
		}
	};

  	//grunt-contrib-clean
  	config.clean = {
  		assets: [dir.web + "/assets/img", dir.web + "/assets/css", dir.web + "/assets/js", "_build"],
  		deploy: ["deploy"],
  	};

  	//grunt-cache-bust
  	/*
  	config.cacheBust = {
  		global_assets:{
  			options: {
  				assets: [
  					'_build/css/app.min.css',
  					'_build/js/app.min.js'
  				],
  				jsonOutput: false,
  				jsonOutputFilename: 'cacheBust.json'
  			},
  			src: ['app/resources/views/base.html.twig']
  		}
  	}
  	*/

  	//grunt-contrib-copy
	config.copy = {
		img: {
			files: [
				{expand: true, cwd: '_assets/img/', src: ['**'], dest: dir.web + '/assets/img'},
				{expand: true, cwd: '_assets/img/', src: ['favicon.ico'], dest: dir.web + '/'}
			]
		},
		js: {
			files: [
				{expand: true, cwd: '_build/js/', src: ['app.min.js'], dest: dir.web + '/assets/js'}
			]
		},
		css: {
			files: [
				{expand: true, cwd: '_build/css/', src: ['app.min.css'], dest: dir.web + '/assets/css'}
			]
		},
		fonts: {
			files: [
				{expand: true, cwd: '_assets/fonts/', src: ['**'], dest: dir.web + '/assets/fonts'}
			]
		}
	};

	//grunt-replace
	config.replace = {
		unix_build_date: {
			options: {
				patterns: [{
					match: /unix_build_date:[\s-]*[\d-]*/g,
					replacement: function() {
			  			return ('unix_build_date: ' + timestamp);
					}
				}]
			},
			files: [
				{expand: true, flatten: true, src: ['app/config/config.yml'], dest: 'app/config/'}
			]
		}
	};

	//grunt-exec
	config.exec = {
		server: 'start cmd /k php app/console server:run --docroot=public_html',
		clear_cache: 'php app/console cache:clear --env=prod --no-debug',
		mk_deploy: 'mkdir deploy',
		install_assets: 'php app/console assets:install deploy',
		assetic_dump: 'php app/console assetic:dump deploy',

		registeradmin: 'php app/console fos:user:create ' + adminUser.name + ' ' + adminUser.email + ' ' + adminUser.password + ' --super-admin',
	};

	//grunt-contrib-watch
	config.watch = {
		scss: {
			files: ['_assets/**/*.*'],
			tasks: ['build'],
		},
	};

  	// # CONFIGURE GRUNT TASKS ---------------------------------
  	//----------------------------------------------------------
  	module.exports = function(grunt) {
  		// show elapsed time at the end
  		//require('time-grunt')(grunt);
  		require('load-grunt-tasks')(grunt);

    	config.pkg = grunt.file.readJSON('package.json');
		grunt.initConfig(config);

		//task - build
		grunt.registerTask('build', [
			'clean:assets',
			'concat:sass',
			'sass:dist',
			'concat:js',
			'concat:css',
			'uglify:js',
			'cssmin',
			/*'cacheBust:global_assets',*/
			'copy',
			'replace:unix_build_date',
			'watch'
		]);

		//task - watch
		/*
		grunt.registerTask('watch', [
			'watch:grunt'
		]);
		*/

		grunt.registerTask('registeradmin', [
			'exec:registeradmin',
		]);

		//task - deploy
		/*
		grunt.registerTask('deploy', [
			'clean:deploy',
			'exec:mk_deploy',
			'exec:clear_cache',
			'exec:install_assets',
			'exec:assetic_dump',
			'copy:deploy',
		]);
		*/

		//task - server
		grunt.registerTask('server', [
			'exec:server',
		]);

	    //task - default
	    grunt.registerTask('default', [
			'server',
			'build'
	    ]);
  	};

}());